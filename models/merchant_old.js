/*
	MERCHANT
	Model and methods associated with merchants.
 */

var Mongoose = require ('mongoose');

var hngre_copy = Mongoose.createConnection("mongodb://localhost/hngre_copy")

var MerchantSchema = new Mongoose.Schema ({
	merchant_name : String,
	merchant_yelp_rating : String,
	merchant_yelp_review_count : String,
	merchant_cuisine1 : String,
	merchant_cuisine2 : String,
	merchant_cuisine3 : String,
	merchant_tel : String,
	merchant_email : String,
	merchant_old_email : String,
	merchant_weburl : String,
	merchant_street_address : String,
	merchant_locality : String,
	merchant_region : String,
	merchant_zip : String,
	merchant_street_address_landmark : String,
	merchant_neighborhood1 : String,
	merchant_neighborhood2 : String,
	merchant_neighborhood3 : String,
	merchant_transit1 : String,
	merchant_transit2 : String,
	merchant_transit3 : String,
	merchant_hours : String,
	merchant_parking : String,
	merchant_credit_card_accept_status : String,
	merchant_price_range : String,
	merchant_attire : String,
	merchant_good_for_groups : String,
	merchant_good_for_kids : String,
	merchant_takes_reservations : String,
	merchant_delivery : String,
	merchant_takeout : String,
	merchant_table_service : String,
	merchant_outdoor_seating : String,
	merchant_wifi : String,
	merchant_good_for_meal : String,
	merchant_music : String,
	merchant_best_nights : String,
	merchant_happy_hours : String,
	merchant_alcohol : String,
	merchant_alcohol_byo : String,
	merchant_smoking : String,
	merchant_coat_check : String,
	merchant_noise_level : String,
	merchant_dancing : String,
	merchant_ambience : String,
	merchant_tv : String,
	merchant_caters : String,
	merchant_wheelchair : String,
	merchant_tags : String,
	merchant_influencers : String,
	merchant_number_of_dishes : String,
	merchant_type : String,
	merchant_dishes : String,
	merchant_zagat_rating_food : String,
	merchant_zagat_rating_decor : String,
	merchant_zagat_rating_service : String,
	merchant_facebook : String,
	merchant_twitter : String,
	merchant_longtitude : String,
	merchant_latitude : String,
	merchant_contact_person_name : String,
	merchant_contact_person_phone : String,
	merchant_contact_person_email : String,
	merchant_contact_person_designation : String,
	merchant_source : String,
	merchant_yelp_link : String,
	merchant_status : String,
	merchant_unique_url : String,
	merchant_hashed_unique_id : String,
	merchant_photo : String
}, {collection: 'merchants'});



MerchantSchema.statics.search = function (params, callback) {
	this.find (params, function (err, merchants) {
		if (err) {
			return callback (err);
		} else {
			callback (null, merchants);
		}
	});
}

function extractTimeSheet (timeString) {
	
}

var Merchant = hngre_copy.model('Merchant', MerchantSchema);


module.exports = Merchant