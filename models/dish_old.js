/*
	DISH
	Model and methods associated with the dish.
 */

var Mongoose = require ('mongoose');

var DishSchema_old = new Mongoose.Schema ({
	dish_name : String,
	dish_cuisine : String,
	dish_sub_cuisine : String,
	dish_primary_ingredient : String,
	dish_secondary_ingredient : String,
	dish_course : String,
	dish_fat : String,
	dish_chilly : String,
	dish_texture : String,
	dish_cookedness : String,
	dish_sweet : String,
	dish_sour : String,
	dish_umami : String,
	dish_tags : String,
	dish_influencers : String,
	dish_merchant_id : {type: String, ref: 'Merchants'},
	dish_photo : String,
	dish_type : String,
	dish_approve_status : String
}, {collection: 'dishes'});

var Dish_old = Mongoose.model('Dish', DishSchema_old);

module.exports = Dish_old;