/**
 * Created by Partinder on 3/31/15.
 */

/**
 * Created by Partinder on 1/27/15.
 */

var Mongoose = require("mongoose")
var Dish = require("../models/dish.js")
var ObjectId = Mongoose.Schema.ObjectId


var restaurantschema = new Mongoose.Schema({

})


//var timeslotschema = new Mongoose.Schema({
//    close: Boolean,
//    hour_24: Boolean,
//    from_time : String,
//    to_time: String
//})
//
//timeslotschema.pre("save",  function next(){
//
//})
//
//var timesheetschema = new Mongoose.Schema({
//    sunday: [timeslotschema],
//    monday:[timeslotschema],
//    tuesday:[timeslotschema],
//    wednesday:[timeslotschema],
//    thrusday:[timeslotschema],
//    friday:[timeslotschema],
//    saturday:[timeslotschema]
//})
//
//timesheetschema.pre("save", function next(){
//
//})




var LocationSchema = new Mongoose.Schema({

    yelp : {
        link: String,
        rating: Number,//{type: Number,get: getPrice, set: setPrice},
        review_count : Number
    },
    contact : {
        primary_email: String,
        email: [String],
        phone: [String]
    },
    location:{ index: "2dsphere", type: {}},
    contact_person:[{
        name: String,
        email: String,
        phone: String,
        designation: String
    }],


    address:{
        street_address: {type: String, index: true},
        locality: {type: String, index: true},
        region: {type: String, index: true},
        zip: {type: String, index: true},
        landmark: String,
        neighbourhood : {type: [String], index: true}
    },
    info: {
        hours:{
            mon:[],
            tue:[],
            wed:[],
            thu:[],
            fri:[],
            sat:[],
            sun:[]

        },
        parking: [String],
        credit_card: Boolean,
        price_range: String,
        attire: String,
        groups: Boolean,
        kids: Boolean,
        reservations: Boolean,
        delivery: {type: Boolean, index: true},
        takeout: {type: Boolean, index: true},
        table_service: Boolean,
        outdoor_seating: Boolean,
        wifi: Boolean,
        meal: [String],
        music: String,
        best_nights: [String],
        alcohol: String,
        alcohol_byo: Boolean,
        smoking: Boolean,
        coat: Boolean,
        noise: String,
        dancing: String,
        ambience: [String],
        tv: Boolean,
        caters: Boolean,
        merchant_type: [String],
        wheelchair: Boolean,
        happyhours: {}
    },
    zagat: {
        food: Number,
        decor: Number,
        service: Number
    },
    social:{
        facebook: String,
        twitter: String,
        instagram: String
    },
    transit: [String],
    tags: {type:[String], index:true},
    influencers: [String],

    status: { type: String, default : "pool"},
    photo: String,

    hngre_url: String,
    linked_to: ObjectId

})

function getPrice(num){
    return (num/10).toFixed(1);
}

function setPrice(num){
    return num*10;
}





var MerchantSchema = new Mongose.Schema({
    name:{type: String, index: true},
    cuisine : {type: [String], index: true},
    dishes:[Dish.Schema],
    url: String,
    source: String,
    branches: [LocationSchema]

})


var Merchant = Mongoose.model("Merchant", MerchantSchema)


//var timesheet = db_hngre.model("timesheet", timesheetschema)

module.exports.merchant = Merchant
module.exports.merchantschema = MerchantSchema