var mongoose = require ('mongoose');

var UserSchema = new mongoose.Schema({

	name: String,
	first_name: String,
	last_name: String,
	email: {type: String, index: {unique: true, required: true}},
	birthday: Date,
	gender: String,
	loc: Array,
	veg_scale: {type: Array, default: [1,2,3,4,5]},
	login: [{
		loginType: String,
		loginDate: Date,
		loginData: {}
	}],
	created: {type: Date, default: Date.now}
});

/**
 * Creates or updates profile. Convenience method for logging in with social media.
 * @param user
 * @param callback
 */
UserSchema.statics.createOrUpdate = function (user, callback)
{
	if (user.email) 
	{
		User.findOneAndUpdate({email: user.email}, user, function (err, foundUser) 
		{
			if (err) {
				callback (err);
				return;
			} else {
				console.log ('User found', foundUser);
				if (foundUser) {
					var returnUser = foundUser;
					callback (null, returnUser);
				} else {
					var newUser = new User(user);
					console.log('Creating new user ', newUser);
					newUser.save(function (err, savedUser) {
						if (err) {
							callback (err);
							return;
						} else {
							var returnUser = savedUser;
							returnUser.rateCount = 0;

							console.log('returning saved user', returnUser);
							callback (null, returnUser);
						}
					});
				}
			}
		});
	}
	else {
		callback (new Error('Email is missing'));
	}
}

UserSchema.statics.createNew = function (user, callback)
{
	console.log('inserting: ', user);
	var newUser = new this(user); //TODO Test this
	newUser.save(function (err, user)
	{
		if (err){
			console.error(err);

			var errMsg;
			errMsg.status = 0;

			if (err.name === 'ValidationError')
			{
				
			}

			return callback (err);
		} 
		return callback (null, user);
	});
}

UserSchema.statics.updateDetails = function (user, callback) {

	if (!user._id) {
		callback (new Error('ID is required to update details'));

	} else {

		this.findOneAndUpdate(user._id, user, function (err, user) {
			if (err) {
				return callback (err);
			} else {
				return callback (null, user);
			}
		});
	}

}
var User = mongoose.model('User', UserSchema);

module.exports = User;
