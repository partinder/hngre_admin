/*
    RECOMMENDATION
    Models and methods associated with recommendations.
 */

var Mongoose = require('mongoose');

var RecommendationSchema = new Mongoose.Schema({
    _user: {type: Mongoose.Schema.Types.ObjectID, ref: 'User'},     //User ID for whom recommendation is generated
    dishes: [{                                                      //List of dishes recommended
        _dish: {type: Mongoose.Schema.Types.ObjectID, ref: 'Dish'}, //|-> ID of dish
        predicted_rating: Number                                    //|-> recommendation score
    }],
    _previous: Mongoose.Schema.Types.ObjectID,                      //ID of previous recommendation in this series
    params: {},                                                     //Parameters on basis of which dishes are selected
    time_sent: {type: Date, default: Date.now()},                   //Time at which reply sent to user
    time_taken: Number                                              //Time taken from request to reply
});

var Recommendation = Mongoose.model('Recommendation', RecommendationSchema);

module.exports = Recommendation;