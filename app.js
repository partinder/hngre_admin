var fs = require("fs")
var https = require('https');
var http = require("http")
var privateKey = fs.readFileSync('keys/hngre_com.key', 'utf8');
var certificate = fs.readFileSync('keys/hngre_com.crt', 'utf8');
var ca = fs.readFileSync('keys/mydomain.ca-bundle', 'utf8');
var credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca
};
var async = require("async")
var config = require("./config")

var express = require('express');
var app = express(); // Https
var app1 = express() // Http

var path = require('path');
var favicon = require('serve-favicon');
//var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require("./methods/auth")
var expressSession = require("express-session")
var flash = require("connect-flash")
var mongoose = require("mongoose")
var multer = require("./methods/multer")
var compression = require("compression")
var methodOverride = require('method-override')
//var logger = require("./methods/winston")
var j = require("./methods/scheduler")
var elasticSync = require("./elastic/elasticsync")


// Setup scheduler


// Routes Handlers
var index = require("./handlers/index.js")
var dashboard_user = require("./handlers/dashboard_user.js")
var crawled_merchant = require("./handlers/crawled_merchant.js")
var open_merchant = require("./handlers/open_merchant.js")
var completed_merchant = require("./handlers/completed_merchant.js")
var search = require("./handlers/search")
var merchant = require("./handlers/merchant.js")
var dish = require("./handlers/dish.js")
var new_dish = require("./handlers/new_dish")
var reviews = require("./handlers/reviews")
var authentication = require("./methods/authentication.js")

var nodemailer = require("nodemailer")


// create reusable transporter object using SMTP transport





//var cache = require('express-redis-cache')({ client: require('redis').createClient() });

//mongoose.connection.close()

// Connect to Db
mongoose.connect("mongodb://"+config.db+"/db_hngre", function(err) {
    if (err) {
        //logger.error(err)
    } else {
        console.log("Connected to Mongodb")
        elasticSync(function(err) {
            if (err) {
                console.log("Error Syncing Elastic")
            } else {
        // view engine setup
        app.set('views', path.join(__dirname, 'views'));
        app.set('view engine', 'ejs');

        // uncomment after placing your favicon in /public
        //app.use(favicon(__dirname + '/public/favicon.ico'));
        //app.use(logger('dev'));

        // Order of Use is very important
        app.use(compression())
        app.use(express.static(path.join(__dirname, 'html/new3')));
        app.use(express.static(path.join(__dirname, 'public')));
        app.use(methodOverride('_method'))
        app.use(bodyParser.urlencoded({
            extended: true
        }))
        app.use(bodyParser.json())
        app.use(cookieParser())
        app.use(expressSession({
            secret: process.env.SESSION_SECRET || "secret",
            resave: false,
            saveUninitialized: false
        }))

        app.use(passport.initialize())
        app.use(passport.session())

        app.use(flash())

        // Specifiy Multer Setting
        app.use(multer)


        // End Use

        // Define routes

        // routes to serve the static HTML files
        app1.get("*", function(req, res) {
            res.redirect("https://www.hngre.com" + req.url)
        })

        app.get('/', function(req, res) {
            console.log("Sent Home")
            res.sendfile('./html/new3/' + 'index.html');
        });

        app.get('/y2a6fc', function(req, res) {
            res.sendfile('./html/new/' + 'betalist.html');
        });

        app.get('/test', function(req, res) {
            res.sendfile('./html/new/' + 'test.html');
        })

        app.get("/maps", function(req, res) {
            res.render("maps")
        })

        app.get("/phonesend", function(req, res) {
            console.log(req.query)

            if (req.query.access_token != "druanal77") {
                res.status(410).send({
                    err: "Wrong access token"
                })
            } else {
                var client = require('twilio')('AC59b515897f70f729a9adf8d8d4f4938f', '24574be3b963db9b7cbf8024d59354d2');

                //Send an SMS text message

                async.parallel([
                    function(callback) {
                        client.sendMessage({

                            to: '+919953640877', // Any number Twilio can deliver to
                            from: '+16466473426', // A number you bought from Twilio and can use for outbound communication
                            body: 'Recommendations not Sent TAKE URGENT ACTION' // body of the SMS message

                        }, function(err, responseData) { //this function is executed when a response is received from Twilio

                            if (!err) { // "err" is an error received during the request, if any

                                // "responseData" is a JavaScript object containing data received from Twilio.
                                // A sample response from sending an SMS message is here (click "JSON" to see how the data appears in JavaScript):
                                // http://www.twilio.com/docs/api/rest/sending-sms#example-1

                                console.log(responseData.from); // outputs "+14506667788"
                                console.log(responseData.body); // outputs "word to your mother."
                                callback(null, '+919953640877')
                            } else {
                                //console.log(err)
                                //res.status(400).send({err:err})
                                callback(err)
                            }
                        });

                    },
                    function(callback) {

                        client.sendMessage({

                            to: '+919873756056', // Any number Twilio can deliver to
                            from: '+16466473426', // A number you bought from Twilio and can use for outbound communication
                            body: 'Recommendations not Sent TAKE URGENT ACTION' // body of the SMS message

                        }, function(err, responseData) { //this function is executed when a response is received from Twilio

                            if (!err) { // "err" is an error received during the request, if any

                                // "responseData" is a JavaScript object containing data received from Twilio.
                                // A sample response from sending an SMS message is here (click "JSON" to see how the data appears in JavaScript):
                                // http://www.twilio.com/docs/api/rest/sending-sms#example-1

                                console.log(responseData.from); // outputs "+14506667788"
                                console.log(responseData.body); // outputs "word to your mother."
                                callback(null, '+919873756056')
                            } else {
                                //console.log(err)
                                //res.status(400).send({err:err})
                                callback(err)
                            }
                        });

                    }
                ], function(err, result) {
                    if(err)
                    {
                        console.log(err)
                        res.status(400).send({err:rr})

                    }                        
                    else
                    {
                        console.log(result)
                        res.status(200).send({err:null})
                    }


                })

            }

        })

        //app.post("/2v7g0hd0cp",function(req,res){
        //    var transporter = nodemailer.createTransport({
        //        service: 'Gmail',
        //        auth: {
        //            user: 'hngremailer@gmail.com',
        //            pass: 'ASDF!#%&'
        //        }
        //    });
        //
        //    var mailOptions = {
        //        from: 'Hngre Mailer  <hngremailer@gmail.com', // sender address
        //        to: 'rohit@hngre.com', // list of receivers
        //        subject: 'New Betalist '+req.body.type+'r : '+req.body.data.email, // Subject line
        //        text: req.body.data.email // plaintext body
        //        //html: '<b>Hello world ✔</b>' // html body
        //    };
        //
        //    // send mail with defined transport object
        //
        //        transporter.sendMail(mailOptions, function(error, info){
        //            if(error){
        //                console.log(err)
        //                res.status(500)
        //                transporter.close()
        //                return
        //            }
        //            console.log('Message sent: ' + info.response);
        //            res.status(200)
        //            transporter.close()
        //        });
        //
        //    //
        //    //Start email
        //})

        // All Routes

        app.get("/rating", authentication.ensureAuthenticated, function(req, res) {
            reviews.get_rating(req, res)


        })
        app.get("/admin", function(req, res) {

            //cache.get(function (error, entries) {
            //    if ( error ) throw error;
            //
            //    entries.forEach(console.log.bind(console));
            //});

            index.get_handle(req, res)

        })
        app.post("/admin", passport.authenticate("login-local", {
            failureRedirect: '/admin'
        }), function(req, res) {

            //req.originalUrl

            index.post_handle(req, res)
        })


        //User Dashboard
        app.get("/admin/user/dashboard", authentication.ensureAuthenticated, function(req, res) {
            dashboard_user.get_handle(req, res)
        })

        app.post("/admin/user/dashboard", function(req, res) {
            dashboard_user.post_handle(req, res)

        })

        app.get("/admin/merchants/reviews", authentication.ensureAuthenticated, function(req, res) {
            //dish.get_handle(req,res)
            //console.log(req.query.count)
            reviews.get_handle(req, res)
        })

        app.get("/admin/merchants/review_user/:id", authentication.ensureAuthenticated, function(req, res) {
            //dish.get_handle(req,res)
            //console.log(req.query.count)
            //reviews.get_handle(req,res)
            reviews.get_handle_user(req, res)
        })

        //Merchant Dashboard
        app.get("/admin/merchants/dashboard", authentication.ensureAuthenticated, function(req, res) {
            res.send("OK")
        })

        // Crawled
        app.get("/admin/merchants/crawled", authentication.ensureAuthenticated, function(req, res) {

            //console.log("here")

            crawled_merchant.get_handle(req, res)
        })

        app.post("/admin/merchants/crawled", function(req, res) {
            crawled_merchant.post_handle(req, res)

        })


        //Open
        app.get("/admin/merchants/open", authentication.ensureAuthenticated, function(req, res) {

            open_merchant.get_handle(req, res)
        })

        app.get("/admin/merchants/completed", authentication.ensureAuthenticated, function(req, res) {

            completed_merchant.get_handle(req, res)
        })

        // Type Ahead
        app.get("/search", authentication.ensureAuthenticated, function(req, res) {

            search.get_handle(req, res)
        })




        //Merchant page
        app.get("/admin/merchants/:id", authentication.ensureAuthenticated, function(req, res) {


            merchant.get_details(req, res)

        })

        app.post("/admin/merchants/:id", authentication.ensureAuthenticated, function(req, res) {
            merchant.post_details(req, res)
        })

        //Dish Page
        app.get("/admin/merchants/dishes/:dish_id", authentication.ensureAuthenticated, function(req, res) {

            //console.log("Cahcing")
            dish.get_handle(req, res)
        })

        app.post("/admin/merchants/dishes/:dish_id", authentication.ensureAuthenticated, function(req, res) {


            dish.post_handle(req, res)

        })

        app.delete("/admin/merchants/dishes/:dish_id", authentication.ensureAuthenticated, function(req, res) {
            dish.delete_handle(req, res)
        })


        // New Dish
        app.get("/admin/merchants/dishes/new_dish/:merchant_id", authentication.ensureAuthenticated, function(req, res) {
            //res.send("OK")
            new_dish.get_handle(req, res)
        })
        app.post("/admin/merchants/dishes/new_dish/:merchant_id", authentication.ensureAuthenticated, function(req, res) {
            //res.send("OK")
            new_dish.post_handle(req, res)
        })

        app.get("admin/merchants/dishes", authentication.ensureAuthenticated, function(req, res) {
            //dish.get_handle(req,res)
        })





        app.get("/404", function(req, res) {

            res.render("404")
        })




        // Logout

        app.get("/logout", function(req, res) {

            req.logout()
            res.redirect("/admin/")
        })


        // catch 404 and forward to error handler
        app.use(function(req, res, next) {
            var err = new Error('Not Found');
            err.status = 404;
            logger.info("404 Error")
            console.log(req.originalUrl)
            //next(err);
            res.render("404")

        });

        // error handlers

        // development error handler
        // will print stacktrace
        if (app.get('env') === 'development') {

            //logger.transports.console.level = 'silly';
            //logger.transports.dailyRotateFile.level = 'error';
            //logger.transports.logentries.level = "error"

            app.use(function(err, req, res, next) {
                res.status(err.status || 500);
                res.render('error', {
                    message: err.message,
                    error: err
                });
            });
        }

        // production error handler
        // no stacktraces leaked to user
        app.use(function(err, req, res, next) {
            res.status(err.status || 500);
            res.render('error', {
                message: err.message,
                error: {}
            });
        });

        var port = process.env.PORT || 3000;


        var httpserver = http.createServer(app)
            //var httpsserver = https.createServer(credentials,app)
        httpserver.listen(3000, function() {
            console.log("Http Server Running on port 3000")
        })

        //httpsserver.listen(3001,function(){
        //    console.log("Https server running on 3001")})
        //app.listen(port, function () {

        //    //var host = server.address().address
        //    //var port = server.address().port
        //    console.log("Service Started at http://localhost:%s", port)
        //    logger.info("Trying loggin with both Winstrom and Logentries")
        //})

            } // END ELSE

        })
    }
})

// Sync Elastic Search Data




module.exports = app;