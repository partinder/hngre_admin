/**
 * Created by Partinder on 1/30/15.
 */

var merchant= require("../models/merchant.js").merchant


module.exports = {

    get_all : function(sort_by,search_query,current_page,status,callback){

        // sort_by is the sort criteria, is a string
        //search_query is the query to be searched
        //current page is the page no of the page sending the request, to be used for skip in DB

        var query ={}


        if(search_query){

            var query = {$or :[{"name" : search_query},{"address.zip" : search_query},{"address.neighbourhood": search_query}]}
        }

        current_page = current_page-1
        var skip_number = current_page * 10


        merchant.find(query).count().where({status : status}).exec(function(err, total_merchants){ // Query Db to get the No. of Merchants

            console.log("Total merchants are :%s",total_merchants)
        // define the sorting criteria
            var sort = {}
            sort[sort_by] = -1
            //console.log(sort)

            var db_query =  merchant.find(query)
                .where({status : status})
                .sort(sort)
                .limit(10)
                .skip(skip_number)
                //.count({applySkipLimit : false})

            db_query.exec(function(err,merchants){
                if(err)
                {

                }
                else
                {
                    //console.log(merchants)
                    callback(merchants,total_merchants)
                }


            })

    })

    },    // end get_all

    get_one : function(id,callback){

        merchant.findOne({_id:id}, function(err,merchant) {
            if (err) {
            }
            else
            {
                callback(merchant)
            }

        })//end findONE
    }//end get_one
}