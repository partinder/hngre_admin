var models = require("../models/adminuser.js")
var flash = require("connect-flash")

module.exports = {

    verifyCredentialslogin : function (req, username, password, done){

    //var user = models.user
    //console.log(models.user)
        models.adminuser.findOne({ username : username}, function(err, user){
            if(err) { done (err)}

            if(!user)
            {
                console.log("No User")

                return done(null, false, req.flash("loginMessage","User Does not exist"))
            }

            user.comparePassword(password, function(err, isMatch) {
                if (err) return done(err);
                if(isMatch)
                {
                    console.log("user found")
                    return done(null, user);
                }
                else
                {
                    return done(null, false, req.flash("loginMessage","Invalid Password"));
                }

            })

        })
    },

    ensureAuthenticated :  function (req, res, next){
        if(req.isAuthenticated())
        {
            //console.log("authenticated")

            //if(req.originalUrl)
            //{
            //    res.redirect(req.originalUrl)
            //}
            next()
        }
        else
        {
            console.log("Not Supposed to be here")
            res.redirect("/admin")
        }
    }


}

