/**
 * Created by Partinder on 2/14/15.
 */

module.exports ={

    check : function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true;
}
}
