/**
 * Created by Partinder on 2/23/15.
 */

var multer = require("multer")

module.exports = multer({
    dest: 'public/uploaded/tmp',
    rename: function (fieldname, filename) {
        return Date.now();
    },
    onFileUploadStart: function (file) {
        console.log(file.originalname + ' is starting ...')
    },
    onFileUploadComplete: function (file) {
        console.log(file.fieldname + ' uploaded to  ' + file.path)
        done = true;
    }
})