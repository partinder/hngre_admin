/**
 * Created by Partinder on 2/23/15.
 */

var passport = require("passport"),
    passportLocal = require("passport-local"),
    authentication = require("./authentication"),
    models = require("../models/adminuser")

passport.use("login-local", new passportLocal.Strategy({passReqToCallback: true}, authentication.verifyCredentialslogin))

passport.serializeUser(function (user, done) {

    //console.log(user)

    done(null, user._id)
})

passport.deserializeUser(function (id, done) {


    models.adminuser.findOne({_id: id}, function (err, user) {
        if (err) {
            return done(null, false)
        }
        else {
            //console.log(user)
            return done(null, user)
        }
    })

})

module.exports = passport
