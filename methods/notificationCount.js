(function() {
    'use strict';
    var mongojs = require('mongojs');
    var async = require('async');
    var _ = require('underscore');

    var nodemailer = require('nodemailer');
    var mailgun = require('nodemailer-mailgun-transport');
    var config = require("../config")

    var transporter = nodemailer.createTransport(mailgun({
        auth: {
            api_key: 'key-a184af3ea4a06a679240732867d0435d',
            domain: 'hngremail.com'
        }
    }));

    module.exports = function() {

        //production db: 52.74.138.138
        var db = mongojs('mongodb://' + config.db + '/db_hngre_notifier', ['actions']);
        db.on('error', function(error) {
            console.log("database error", error);
        });
        db.on('connect', function() {
            console.log("database connected");
        });
        var yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        var today = new Date();
        today = today.toISOString();
        yesterday = yesterday.toISOString();
        console.log(today, yesterday);

        db.actions.aggregate([{
            $match: {
                resolved: {
                    $gte: new Date(yesterday),
                    $lt: new Date(today)
                }
            }
        }, {
            $project: {
                email: 1,
                phone: 1,
                deviceIds: 1,
                response: 1,
                id: 1
            }
        }, {
            $group: {
                _id: {
                    event: "$id"
                },
                emails: { $addToSet: { email: "$email", status: "$response.message" } },
                phones: { $addToSet: { phone: "$phone", status: "$response.status" } },
                devices: { $addToSet: { deviceIds: "$deviceIds", status: "$response.success" } }
            }
        }, {
            $project: {
                event: "$_id.event",
                _id: 0,
                emails: {
                    $cond: {
                        if: {
                            $ne: ["$emails", [{}]]
                        },
                        then: {
                            $map: {
                                input: "$emails",
                                as: "email",
                                in : {
                                    email: "$$email.email",
                                    status: {
                                        $cond: {
                                            if: {
                                                $eq: ["$$email.status", "Queued. Thank you."]
                                            },
                                            then: "success",
                                            else: "failure"
                                        }
                                    }
                                }
                            }
                        },
                        else: false
                    }
                },
                phones: {
                    $cond: {
                        if: {
                            $ne: ["$phones", [{}]]
                        },
                        then: {
                            $map: {
                                input: "$phones",
                                as: "phone",
                                in : {
                                    phone: "$$phone.phone",
                                    status: {
                                        $cond: {
                                            if: {
                                                $eq: ["$$phone.status", "queued"]
                                            },
                                            then: "success",
                                            else: "failure"
                                        }
                                    }
                                }
                            }
                        },
                        else: false
                    }
                },
                devices: {
                    $cond: {
                        if: {
                            $ne: ["$devices", [{}]]
                        },
                        then: {
                            $map: {
                                input: "$devices",
                                as: "device",
                                in : {
                                    device: "$$device.deviceIds",
                                    status: {
                                        $cond: {
                                            if: {
                                                $eq: ["$$device.status", 1]
                                            },
                                            then: "success",
                                            else: "failure"
                                        }
                                    }
                                }
                            }
                        },
                        else: false
                    }

                }
            }
        }], function(err, actions) {
            if (err) {
                console.log(err);
                return;
            }
            actions = actions.map(function(action) {
                var data = { event: action.event };
                if (_.isArray(action.emails)) {
                    data.emails = action.emails;
                }
                if (_.isArray(action.phones)) {
                    data.phones = action.phones;
                }
                if (_.isArray(action.devices)) {
                    data.devices = action.devices;
                }
                return data;
            });
            if (actions && actions.length && actions.length > 0) {
                console.log("Total Notifications", actions.length);
                var mailOptions = {
                    from: 'info@hngremail.com', // sender address
                    to: 'suresh.mahawar@hngre.com, partinder@hngre.com, rohit@hngre.com', // list of receivers
                    subject: 'Today total notification events triggered is ' + actions.length, // Subject line
                    html: arrayToTabel(actions)
                };
                transporter.sendMail(mailOptions, function(err, res) {
                    if (err) {
                        console.log(err);
                        return;
                    }
                    console.log(res);
                });
            } else {
                console.log('Ops Notifications found');
                var mailOptions = {
                    from: 'info@hngremail.com', // sender address
                    to: 'suresh.mahawar@hngre.com, partinder@hngre.com, rohit@hngre.com', // list of receivers
                    subject: 'Today notification events has not been triggered', // Subject line
                    text: 'Ops Notifications found'
                };
                transporter.sendMail(mailOptions, function(err, res) {
                    if (err) {
                        console.log(err);
                        return;
                    }
                    console.log(res);
                });
            }
        })

        function arrayToTabel(arr) {
            var html = "";
            html += '<table border="1" cellspacing="1" cellpadding="5">';
            for (var i = 0; i < arr.length; i++) {
                var ecount = arr[i].emails && arr[i].emails.length && arr[i].emails.length > 0 ? arr[i].emails.length : 0;
                var pcount = arr[i].phones && arr[i].phones.length && arr[i].phones.length > 0 ? arr[i].phones.length : 0;
                var dcount = arr[i].devices && arr[i].devices.length && arr[i].devices.length > 0 ? arr[i].devices.length : 0;
                html += "<tr><td>Event " + arr[i].event + " is:</td>";
                html += "<td> emails: " + ecount + "</td>";
                html += "<td> messages : " + pcount + "</td>";
                html += "<td> push notications : " + dcount + "</td></tr>";
            }
            html += '</table>';

            html += '<table border="1" cellspacing="1" cellpadding="5">';
            for (var i = 0; i < arr.length; i++) {
                html += "<tr><td>Event " + arr[i].event + " is:</td>";
                html += "<td>" + innerTable(arr[i]) + "</td></tr>";
            }
            html += '</table>';
            return html;
        }

        function innerTable(arr) {
            if (arr.emails) {
                var html = "";
                html += '<table border="1" cellspacing="1" cellpadding="5">';
                for (var i = 0; i < arr.emails.length; i++) {
                    html += "<tr><td>Email : " + arr.emails[i].email + " is:</td>";
                    html += "<td>" + arr.emails[i].status + "</td></tr>";

                }
                html += '</table>';
                return html;
            } else if (arr.phones) {
                var html = "";
                html += '<table border="1" cellspacing="1" cellpadding="5">';
                for (var i = 0; i < arr.phones.length; i++) {
                    html += "<tr><td>Phone : " + arr.phones[i].phone + " is:</td>";
                    html += "<td>" + arr.phones[i].status + "</td></tr>";

                }

                html += '</table>';
                return html;
            } else if (arr.devices) {
                var html = "";
                html += '<table border="1" cellspacing="1" cellpadding="5">';
                for (var i = 0; i < arr.devices.length; i++) {
                    html += "<tr><td>Device : " + arr.devices[i].device + " is:</td>";
                    html += "<td>" + arr.devices[i].status + "</td></tr>";

                }

                html += '</table>';
                return html;
            } else {
                return "No record";
            }
        }

    }




}());