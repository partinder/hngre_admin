/**
 * Created by Partinder on 2/12/15.
 */

module.exports = {

    remove_duplicates : function (result){

    //console.log(result)
        var answer = []

        if(result === null || result === undefined)
        {
            //result = []
            return answer
        }

        else{
            if(Array.isArray(result))
            {
                result = result.sort( function(a,b){
                    var labelA=a.toLowerCase(), labelB=b.toLowerCase()
                    if (labelA < labelB) //sort string ascending
                        return -1
                    if (labelA > labelB)
                        return 1
                    return 0 //default return value (no sorting)

                })


                for( var i=0; i<result.length-1; i++ ) {
                    if ( result[i]== result[i+1]) {
                        delete result[i];
                    }
                }

                result= result.filter( function( el ){ return (typeof el !== "undefined"); } );

                return result

            }
            else
            {

                return [result]
            }
        }


}
}
