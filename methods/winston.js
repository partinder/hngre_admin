/**
 * Created by Partinder on 2/24/15.
 */
//Setup Logging with Winston and Logentries
var winston = require("winston")
var Logentries = require('winston-logentries');

var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({handleExceptions:true}),
        new (winston.transports.DailyRotateFile)({ filename: 'logs/server.log', datePattern:".dd-MM-yyyy" }),
        new (winston.transports.Logentries)({token: 'd72023ae-6d1d-49ef-adeb-06338f3b8a4d', handleExceptions: true})
    ]
});

// Log Levels

//Lowest - silly
//
//verbose
//info
//http
//warn
//error

//Highest - silent

module.exports = logger