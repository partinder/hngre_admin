/**
 * Created by Partinder on 2/10/15.
 */

var dish= require("../models/dish.js")
var ObjectID = require("mongodb").ObjectID

module.exports ={

    get_all : function(merchant_id,callback){

        if(typeof merchant_id  === "String")
        {
            merchant_id = new ObjectID(merchant_id)
        }

        dish.find({_merchant: merchant_id})
            .where({"approve_status" : 1})
            .select('name')
            .exec(function(err, dishes){
                if(err)
                {

                }
                else
                {

                    callback(dishes)
                }

            })

    },
    get_one: function(dish_id,callback){
        dish
        .findById(dish_id)
            .where({"approve_status" : 1})
        .populate('_merchant','name _id cuisine')
        .exec(function(err,dish){
                if(err)
                {

                }
                else
                {

                    callback(dish)

                }

            })
    }
}
