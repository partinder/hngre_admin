/**
 * Created by Partinder on 2/24/15.
 */
var schedule = require("node-schedule")
var fs = require('fs')
var findRemoveSync = require('find-remove');
var logger = require("winston")
var CronJob = require('cron').CronJob;

var easyZip = require("easy-zip")
var rimraf = require("rimraf")
var s3 = require("s3")
var fs = require("fs")

var spawn = require('child_process').spawn;
var zip = new easyZip.EasyZip();
var now = new Date().getTime().toString()
var localzip = '/home/ubuntu/dumps/db_hngre-' + now + ".zip"

var nodemailer = require("nodemailer")
var hours = require("../scripts/hours")
var elasticSync = require("../elastic/elasticsync")
var dailyUsers = require("../scripts/dailyUsers")
var notificationCount = require("../methods/notificationCount")


// create reusable transporter object using SMTP transport
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'hngremailer@gmail.com',
        pass: 'ASDF!#%&'
    }
});

var mailOptions = {
    from: 'Hngre Mailer  <hngremailer@gmail.com', // sender address
    to: 'partinder@hngre.com', // list of receivers
    subject: "Hngre Databse backup Sucessfull", // Subject line
    text: "Hngre Database sucessfully BackedUp on SINGAPORE SERVER" // plaintext body
        //html: '<b>Hello world ✔</b>' // html body
};

var mailOptions1 = {
    from: 'Hngre Mailer  <hngremailer@gmail.com', // sender address
    to: 'partinder@hngre.com', // list of receivers
    subject: "Merchant Hours Sync'd", // Subject line
    text: "Merchant Hours SYNCD for this Month" // plaintext body
        //html: '<b>Hello world ✔</b>' // html body
};


var j = new CronJob('59 3 * * *', function() {
        /*
         * Runs every weekday (Monday through Friday)
         * at 11:30:00 AM. It does not run on Saturday
         * or Sunday.
         */

        console.log("Started Mongo Dump Cron at " + Date())

        var result = findRemoveSync("logs", {
            age: {
                seconds: 86400
            }
        });
        logger.info("Old log file removed")

        logger.info("Starting Mongo Dump")

        var args = ['--db', 'db_hngre', '--out', '/home/ubuntu/dumps']
        var mongodump = spawn('/usr/bin/mongodump', args);

        mongodump.stdout.on('data', function(data) {
            //console.log('stdout: ' + data);
        });
        mongodump.stderr.on('data', function(data) {
            //console.log('stderr: ' + data);
            console.log(data)
        });
        mongodump.on('exit', function(code) {

            console.log("Mongo Dump Done")

            var zipargs = ['-r', '/home/ubuntu/dumps/db_hngre-' + now + '.zip', '/home/ubuntu/dumps/db_hngre']
            var zipcre = spawn('/usr/bin/zip', zipargs)

            zipcre.stdout.on('data', function(data) {
                console.log(data)

            })
            zipcre.stderr.on('data', function(data) {
                console.log(data)

            })
            zipcre.on('exit', function(code) {

                console.log("Zip file created")

                var remotezip = 'db_hngre-' + now + '.zip'

                var client = s3.createClient({
                    maxAsyncS3: 20, // this is the default
                    s3RetryCount: 3, // this is the default
                    s3RetryDelay: 1000, // this is the default
                    multipartUploadThreshold: 20971520, // this is the default (20 MB)
                    multipartUploadSize: 15728640, // this is the default (15 MB)
                    s3Options: {
                        accessKeyId: "AKIAJLEJXUICVBQU7RMQ",
                        secretAccessKey: "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"
                            // any other options are passed to new AWS.S3()
                            // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Config.html#constructor-property
                    }
                });
                //console.log(localzip)
                var params = {

                    //localFile: localzip,
                    //deleteRemoved: true
                    localFile: localzip,

                    s3Params: {
                        Bucket: "hngrebackups",
                        Key: remotezip

                        // other options supported by putObject, except Body and ContentLength.
                        // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
                    }
                };

                var uploader = client.uploadFile(params);
                //uploader = client.uploadFile(params)
                uploader.on('error', function(err) {
                    console.error("unable to upload:", err.stack);
                });
                uploader.on('progress', function() {
                    logger.info("progress", uploader.progressMd5Amount,
                        uploader.progressAmount, uploader.progressTotal);
                });
                uploader.on('end', function() {


                    rimraf("/home/ubuntu/dumps/db_hngre", function(err) {
                        if (err) {
                            throw err
                            //callback(err)
                        } else {
                            fs.unlink(localzip, function(err) {
                                if (err) {
                                    mailOptions.subject = "FAILED to take Databse backup"
                                    mailOptions.text = "Err : " + err
                                }

                                //logger.info("Mongo Dump Uploaded to S3 and local deleted")
                                //res.send('File uploaded to: ' + target_path + ' - ' + req.files.dish_image.thumbnail.size + ' bytes');

                                elasticSync(function(err) {
                                    if (err) {
                                        console.log("Error Syncing Elastic")
                                    } else {

                                        transporter.sendMail(mailOptions, function(error, info) {
                                            if (error) {
                                                return console.log(error);
                                            }
                                            console.log('Message sent: ' + info.response);
                                        })

                                    }
                                })
                            })
                        }
                    })
                })
            })
        })


    },
    function() {
        /* This function is executed when the job stops */
    },
    true, /* Start the job right now */
    "Asia/Kolkata" /* Time zone of this job. */
);


var k = new CronJob("59 4 1 * *", function() {
        /*
         * Runs every weekday (Monday through Friday)
         * at 11:30:00 AM. It does not run on Saturday
         * or Sunday.
         */

        hours(function(message) {
            console.log(message)
            transporter.sendMail(mailOptions1, function(error, info) {
                if (error) {
                    return console.log(error);
                }
                console.log('Message sent: ' + info.response);
            })
        })
    }, function() {
        /* This function is executed when the job stops */
    },
    true, /* Start the job right now */
    "Asia/Kolkata" /* Time zone of this job. */
);

var l = new CronJob("5 2 * * *", function() {
        dailyUsers()
        notificationCount()

    }, function() {
        /* This function is executed when the job stops */
    },
    true, /* Start the job right now */
    "Asia/Kolkata" /* Time zone of this job. */
);

var crons = []
crons.push(j)
crons.push(k)
crons.push(l)



module.exports = crons