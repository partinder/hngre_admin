/**
 * Created by Partinder on 4/21/15.
 */

var elasticsearch = require("elasticsearch")
var config = require("../config")
var elasticClient = new elasticsearch.Client({
    //log : "trace"
    //host: "http://localhost:9200"
    //host: "192.168.1.4:9200"
    // host: "52.91.167.165:9200"
     //host: "52.91.167.165:9200"
     host: config.elasticsearch
})
var elasticFormat = require("./helpers").elasticFormat


//elasticClient.index({
//    index:"hngre",
//    type:"autocomplete",
//    body:
//    {
//        suggested:
//        {
//            input:["japanese"],
//            output:"Japanese",
//            context:
//            {
//                zip:["10003","10004","10005"],
//                neighbourhood:["East Village","Lower East Side"],
//                locality:"Manhattan",
//                region:"NY",
//                country:"United States",
//                location:[
//                    {
//                        lat:"40.76175596",
//                        lon:"-73.97922993"
//                    },
//                    {
//                        lat:"40.722283",
//                        lon:" -73.98747"
//                    },
//                    {
//                        lat:"40.74140002",
//                        lon:" -73.98815019"
//                    }
//
//                ]
//
//            }
//        }
//    }
//},function(err,res){
//    console.log(res)
//})

//elasticClient.index({
//    index:"hngre",
//    type:"autocomplete",
//    body:{
//        zip:["10003","10004","10006","111111"],
//        neighbourhood:["east village","west village","Tribeca","Noilta","NA"],
//        suggest_field:{
//            input:["ita","italy","italiano"],
//            output:"Italian",
//            payload:{scope:"cuisine"},
//            context:{
//                location:[
//                    {
//                    lat:"40.730961",
//                    lon:"-73.990287"
//                    },
//                    {
//                        lat:"40.721128",
//                        lon:"-73.983933"
//                    },
//                    {
//                        lat:"40.761853",
//                        lon:" -73.964903"
//                    }]
//            }
//        }
//
//    }
//})

//elasticClient.index({
//    index:"hngre",
//    type:"test",
//    body:{
//        name:"Indian",
//        scope:"cuisine",
//        zip:["10001","10006","1005"]
//    }
//})

//elasticClient.indices.putSettings({
//    index:"hngre",
//    body:{
//        "settings": {
//            "analysis": {
//                "filter": {
//                    "autocomplete_filter": {
//                        "type":     "edge_ngram",
//                        "min_gram": 1,
//                        "max_gram": 20
//                    }
//                },
//                "analyzer": {
//                    "autocomplete": {
//                        "type":      "custom",
//                        "tokenizer": "standard",
//                        "filter": [
//                            "lowercase",
//                            "autocomplete_filter"
//                        ]
//                    }
//                }
//            }
//        }
//    }
//},function(err,res){
//    console.log(res)
//})

//elasticClient.deleteByQuery({
//    index:"autocomplete",
//    type:"suggest",
//    body:{
//        query:{
//            match_all:{}
//        }
//    }
//})

//var starttime = new Date().getTime()
//
//elasticClient.search({
//    index:"autocomplete",
//    type:"suggest",
//    body:{
//        query:{
//            filtered:{
//                query:{
//                    match:{
//                        phrase:{
//                            query:"VALUE",
//                            "analyzer": "standard"
//                        }
//                    }
//                },
//                filter:{
//                //    term:{
//                //        zip:["10075","10003","10004"]
//                //    }
//
//                    geo_distance: {
//                        distance: 100,
//                            distance_unit: "km",
//                            "location": {
//                                "lat": 40.73,
//                                "lon": -74.1
//                        }
//                    }
//                }
//            }
//        }
//    }
//},function(err,result){
//    var reply = []
//
//    result.hits.hits.forEach(function(hit){
//
//        reply.push({
//            term:hit._source.phrase,
//            scope:hit._source.scope,
//            type: hit._source.type
//        })
//
//    })
//    var endtime = new Date().getTime()
//    console.log(reply)
//    console.log("Took %s seconds",(endtime-starttime)/1000 )
//})
//
//elasticClient.search({
//    index:"hngre",
//    type:"dishes",
//    body:{
//        query:{
//           filtered:{
//               query:{
//                   term:{
//                       FIELD:{
//                           value:"VALUE"
//                       }
//                   }
//               },
//               filter:{
//                   term:{
//                       FIELD:"VALUE"
//
//                   }
//
//                   //"geo_distance": {
//                   //    "distance": 1000,
//                   //    "distance_unit": "m",
//                   //    "location": {
//                   //        "lat": 40.73,
//                   //        "lon": -74.1
//                   //    }
//                   //}
//               }
//           }
//        }
//    }
//},function(err,result){
//
//    if(err)
//    {
//
//    }
//    else
//    {
//        var reply = []
//
//        if(result.hits.hits.length !=0){
//            result.hits.hits.forEach(function(hit){
//
//                reply.push({
//                    name:hit._source.name,
//                    description:hit._source.description,
//                    photo: hit._source.photo,
//                    merchant:hit._source.merchant
//                })
//
//            })
//            var endtime = new Date().getTime()
//            console.log(reply)
//
//
//        }
//        else
//        {
//            var endtime = new Date().getTime()
//            console.log(result)
//
//        }
//    }
//
//
//})

elasticClient.mget({
    index:"hngre",
    type:"dishes",
    body:{
        "docs" : [
            {
                "_id" : "553df550acbf94330cfc79ea"
            },
            {

                "_id" : "5523d6f04fd0398e5d437761"        }
        ]
    }
},function(err,result){
    console.log(result)
})



//elasticClient.indices.create({
//    index:"hngre"
//}, function(err,res){
//    console.log(res)
//})