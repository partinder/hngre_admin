/**
 * Created by Partinder on 4/16/15.
 */

module.exports = {

    elasticFormat : function(merchant){

        if(merchant.dishes.length !=0){
            var dishes = []
            var dishes_del = []
            merchant.dishes.forEach(function(dish){
                if(dish.approve_status == "true" || dish.approve_status == 1)
                {
                    var dish_ios_photo
                    if(dish.photo){
                        dish_ios_photo = dish.photo.ios
                    }
                    var dish_desc
                    if(dish.description){
                        dish_desc = dish.description
                    }

                    var dish_ingre
                    if(dish.ingredients){
                        dish_ingre = dish.ingredients
                    }

                    var dish_cuisine
                    if(dish.cuisine)
                    {
                        dish_cuisine = dish.cuisine
                    }
                    var dish_tags
                    if(dish.tags)
                    {
                        dish_tags = dish.tags
                    }

                    dishes.push({
                        index: "hngre",
                        type: "dishes",
                        id: dish._id.toString(),
                        body: {
                            name: dish.name,
                            description: dish_desc,
                            veg_type: dish.veg_type,
                            photo: dish_ios_photo,
                            course: dish.course,
                            ingredients: dish_ingre,
                            cuisine: dish_cuisine,
                            tags: dish_tags,
                            merchant: {
                                name: merchant.name,
                                _id:merchant._id,
                                price_range: merchant.price_range,
                                location: {
                                    lat: merchant.location.coordinates[1],
                                    lon: merchant.location.coordinates[0]
                                },
                                phone: merchant.contact.phone,
                                hours: merchant.info.hours,
                                meal:merchant.info.meal,
                                address: {
                                    street_address: merchant.address.street_address, // Thoroughfare
                                    neighbourhood: merchant.address.neighbourhood, // Sublocality
                                    locality: merchant.address.locality, // City (Google)
                                    region: merchant.address.region, // State, Adminstrative area
                                    zip: merchant.address.zip, // Postal Code
                                    landmark: merchant.address.landmark,
                                    country: merchant.address.country
                                }

                            }
                        }
                    })

                }
                else
                {
                    dishes_del.push(dish._id.toString())
                }
            })

            return [dishes,dishes_del]

        }

    }
}