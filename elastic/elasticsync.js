/**
 * Created by Partinder on 4/17/15.
 */

var merchant = require("../models/merchant").merchant
var elasticsearch = require("./search")
var elastic = require("elasticsearch")
var config = require("../config")
var elasticClient = new elastic.Client({
    //log: "trace",
    //host: '54.84.67.184:9200'
    //
    //host: "http://localhost:9200"
    //host: "52.91.167.165:9200"
    //host: "192.168.1.4:9200"
    host: config.elasticsearch // Singapore
})
var bulk_query = []
var _ = require("underscore")

var count = 0
var merchant_count = 0
var async = require("async")
var _ = require("underscore")
var mongoose = require("mongoose")

var typemap = {
    "name": "Dish",
    "merchantName": "Outlet",
    "ingredients": "Ingredient",
    "tags": "Dish",
    "cuisine": "Cuisine"
}

// mongoose.connect("mongodb://localhost/db_hngre", function(err) {
//     if (err) {
//         throw err
//     } else {
//         merchantsync(function() {

//         })
//     }
// })



module.exports = function(callbackMain) {






    elasticClient.deleteByQuery({
        index: "autocomplete",
        type: "suggest",
        body: {
            query: {
                match_all: {}
            }
        }
    })

    console.log("Start Suggest Sync")
    async.parallel({
        cuisine: function(callback) {


            //[
            //    {$match: {status: 'completed'}},
            //    {$project: {_id: 0, 'address.zip': 1, 'address.neighbourhood': 1, 'location': 1, 'address.locality': 1, 'address.region': 1, 'address.country': 1, dishes: 1}},
            //    {$unwind: '$dishes'},
            //    {$unwind: '$dishes.cuisine'},
            //    {$unwind: '$address.neighbourhood'},
            //    {$group: {_id: {cuisine: {$toLower: '$dishes.cuisine'}, country: '$address.country'}, locations: {$addToSet: '$location.coordinates'}, zipcodes: {$addToSet: '$address.zip'}, neighbourhoods: {$addToSet: '$address.neighbourhood'}, localities: {$addToSet: '$address.locality'}, regions: {$addToSet: '$address.region'}}},
            //    {$project: {_id: 0, phrase: '$_id.cuisine', locations: 1, zipcodes: 1, neighbourhoods: 1, localities: 1, country: '$_id.country', regions: 1}}
            //]
            //Cuisine
            merchant.aggregate([{
                $match: {
                    status: 'completed'
                }
            }, {
                $project: {
                    _id: 0,
                    'dishes': 1,
                    'address.zip': 1,
                    'address.neighbourhood': 1,
                    'location': 1,
                    'address.locality': 1,
                    'address.region': 1,
                    'address.country': 1
                }

            }, {
                $unwind: '$dishes'
            }, {
                $unwind: '$dishes.cuisine'
            }, {
                $unwind: '$address.neighbourhood'
            }, {
                $group: {
                    _id: {
                        cuisine: {
                            $toLower: '$dishes.cuisine'
                        },
                        country: '$address.country'
                    },
                    locations: {
                        $addToSet: '$location.coordinates'
                    },
                    zipcodes: {
                        $addToSet: '$address.zip'
                    },
                    neighbourhoods: {
                        $addToSet: '$address.neighbourhood'
                    },
                    localities: {
                        $addToSet: '$address.locality'
                    },
                    regions: {
                        $addToSet: '$address.region'
                    },
                    count: {
                        $sum: 1
                    }
                }
            }, {
                $project: {
                    _id: 0,
                    phrase: '$_id.cuisine',
                    locations: 1,
                    zipcodes: 1,
                    neighbourhoods: 1,
                    localities: 1,
                    country: '$_id.country',
                    regions: 1,
                    count: 1
                }
            }], function(err, results) {
                if (err) {
                    console.log(err)

                } else {
                    callback(null, results)

                }

            })

        },
        merchantName: function(callback) {
            //Merchant Name
            merchant.aggregate([{
                $match: {
                    status: 'completed'
                }
            }, {
                $project: {
                    _id: 0,
                    name: 1,
                    'address.neighbourhood': 1,
                    'address.zip': 1,
                    'address.locality': 1,
                    'address.region': 1,
                    'address.country': 1,
                    location: 1
                }
            }, {
                $unwind: '$address.neighbourhood'
            }, {
                $group: {
                    _id: {
                        name: '$name',
                        country: '$address.country'
                    },
                    locations: {
                        $addToSet: '$location.coordinates'
                    },
                    zipcodes: {
                        $addToSet: '$address.zip'
                    },
                    neighbourhoods: {
                        $addToSet: '$address.neighbourhood'
                    },
                    localities: {
                        $addToSet: '$address.locality'
                    },
                    regions: {
                        $addToSet: '$address.region'
                    },
                    count: {
                        $sum: 1
                    }
                }
            }, {
                $project: {
                    _id: 0,
                    phrase: '$_id.name',
                    locations: 1,
                    zipcodes: 1,
                    neighbourhoods: 1,
                    localities: 1,
                    country: '$_id.country',
                    regions: 1,
                    count: 1
                }
            }], function(err, results) {
                if (err) {
                    console.log(err)
                } else {
                    callback(null, results)
                }


            })

        }

        ,
        // name: function(callback) {
        //     //Dish Name
        //     merchant.aggregate([{
        //         $match: {
        //             status: 'completed'
        //         }
        //     }, {
        //         $project: {
        //             _id: 0,
        //             'dishes.name': 1,
        //             'address.zip': 1,
        //             'address.neighbourhood': 1,
        //             'address.locality': 1,
        //             'address.region': 1,
        //             'address.country': 1,
        //             location: 1
        //         }
        //     }, {
        //         $unwind: '$dishes'
        //     }, {
        //         $unwind: '$address.neighbourhood'
        //     }, {
        //         $group: {
        //             _id: {
        //                 dish: {
        //                     $toLower: '$dishes.name'
        //                 },
        //                 country: '$address.country'
        //             },
        //             locations: {
        //                 $addToSet: '$location.coordinates'
        //             },
        //             zipcodes: {
        //                 $addToSet: '$address.zip'
        //             },
        //             neighbourhoods: {
        //                 $addToSet: '$address.neighbourhood'
        //             },
        //             localities: {
        //                 $addToSet: '$address.locality'
        //             },
        //             regions: {
        //                 $addToSet: '$address.region'
        //             },
        //             count: {
        //                 $sum: 1
        //             }
        //         }
        //     }, {
        //         $project: {
        //             _id: 0,
        //             phrase: '$_id.dish',
        //             locations: 1,
        //             zipcodes: 1,
        //             neighbourhoods: 1,
        //             localities: 1,
        //             country: '$_id.country',
        //             regions: 1,
        //             count: 1
        //         }
        //     }], function(err, results) {
        //         if (err) {
        //             console.log(err)
        //         } else {
        //             callback(null, results)
        //         }

        //     })

        // }

        // ,
        tags: function(callback) {
            //Dish Tags
            merchant.aggregate([{
                $match: {
                    status: 'completed'
                }
            }, {
                $project: {
                    _id: 0,
                    'dishes': 1,
                    'address.zip': 1,
                    'address.neighbourhood': 1,
                    'address.locality': 1,
                    'address.region': 1,
                    'address.country': 1,
                    location: 1
                }
            }, {
                $unwind: '$dishes'
            }, 
            {
                $match : {
                    "dishes.approve_status":true
                }
            },{
                $unwind: '$dishes.tags'
            }, {
                $unwind: '$address.neighbourhood'
            }, {
                $group: {
                    _id: {
                        tag: {
                            $toLower: '$dishes.tags'
                        },
                        country: '$address.country'
                    },
                    locations: {
                        $addToSet: '$location.coordinates'
                    },
                    zipcodes: {
                        $addToSet: '$address.zip'
                    },
                    neighbourhoods: {
                        $addToSet: '$address.neighbourhood'
                    },
                    localities: {
                        $addToSet: '$address.locality'
                    },
                    regions: {
                        $addToSet: '$address.region'
                    },
                    count: {
                        $sum: 1
                    }
                }
            }, {
                $project: {
                    _id: 0,
                    phrase: '$_id.tag',
                    locations: 1,
                    zipcodes: 1,
                    neighbourhoods: 1,
                    localities: 1,
                    country: '$_id.country',
                    regions: 1,
                    count: 1
                }
            }], function(err, results) {
                if (err) {
                    console.log("FOUND ERR")
                    console.log(err)
                } else {
                    callback(null, results)
                }

            })

        }

        ,
        ingredients: function(callback) {
            // Ingredients
            merchant.aggregate([{
                $match: {
                    status: 'completed'
                }
            }, {
                $project: {
                    _id: 0,
                    'dishes': 1,
                    'address.zip': 1,
                    'address.neighbourhood': 1,
                    'address.locality': 1,
                    'address.region': 1,
                    'address.country': 1,
                    location: 1
                }
            }, {
                $unwind: '$dishes'
            }, 
            {
                $match : {
                    "dishes.approve_status":true
                }
            },
            {
                $unwind: '$dishes.ingredients'
            }, {
                $unwind: '$address.neighbourhood'
            }, {
                $group: {
                    _id: {
                        ingredient: {
                            $toLower: '$dishes.ingredients'
                        },
                        country: '$address.country'
                    },
                    locations: {
                        $addToSet: '$location.coordinates'
                    },
                    zipcodes: {
                        $addToSet: '$address.zip'
                    },
                    neighbourhoods: {
                        $addToSet: '$address.neighbourhood'
                    },
                    localities: {
                        $addToSet: '$address.locality'
                    },
                    regions: {
                        $addToSet: '$address.region'
                    },
                    count: {
                        $sum: 1
                    }
                }
            }, {
                $project: {
                    _id: 0,
                    phrase: '$_id.ingredient',
                    locations: 1,
                    zipcodes: 1,
                    neighbourhoods: 1,
                    localities: 1,
                    regions: 1,
                    country: '$_id.country',
                    count: 1
                }
            }], function(err, results) {
                if (err) {
                    console.log(err)
                } else {
                    callback(null, results)
                }

            })

        }
    }, function(err, results) {
        if (err) {
            console.log(err)
        } else {
            var keys = _.keys(results)
                //console.log(keys)
            keys.forEach(function(key) {
                var scope = key
                var type = typemap[scope]

                if (key == "merchantName") {
                    scope = "merchant.name"
                }




                results[key].forEach(function(data) {
                    //console.log(data.country)

                    if (key == "cuisine") {
                        if (data.phrase == "chinese" &&  data.country == "India")
                            console.log(data)
                    }
                    var type1 = type

                    var query = {
                        index: {
                            _index: "autocomplete",
                            _type: "suggest"
                        }
                    }
                    if (key == "merchantName" && data.locations.length > 1) {
                        type1 = data.locations.length + " Outlets"
                    }


                    var locations = []
                    data.locations.forEach(function(loc) {
                        locations.push({
                            lat: loc[1],
                            lon: loc[0]
                        })
                    })
                    if (!data.phrase) {
                        console.log(data, scope)
                    }

                    var body = {
                        phrase: data.phrase.trim(),
                        scope: scope,
                        type: type1,
                        count: data.count,
                        zip: data.zipcodes,
                        neighbourhood: data.neighbourhoods,
                        locality: data.localities,
                        country: data.country,
                        location: locations,
                        region: data.regions

                    }

                    bulk_query.push(query)
                    bulk_query.push(body)
                })

            })

            console.log("Finished Suggest Sync")
            console.log("Query count after Suggest is: %s", bulk_query.length)
            dishsync(function() {

                console.log("Query count after Dish is: %s", bulk_query.length)
                console.log("Finished Dishes Sync")

                locsync(function() {
                    console.log("Query count after Lsuggest is: %s", bulk_query.length)
                    console.log("Finished Lsuggest Sync")
                    merchantsync(function() {

                        console.log("Query count after MerchantSync is: %s", bulk_query.length)
                        console.log("Finished Merchant Sync")

                        elasticsearch.bulkOperation(bulk_query, function(err_elastic) {
                            callbackMain(err_elastic)
                        })

                    })
                })

            })

        }


    })

}


function dishsync(callback) {

    elasticClient.deleteByQuery({
        index: "hngre",
        type: "dishes",
        body: {
            query: {
                match_all: {}
            }
        }
    })

    merchant.aggregate([{
        $match: {
            status: 'completed'
        }
    }, {
        $unwind: '$dishes'
    }, {
        $match: {
            'dishes.approve_status': true
        }
    }, {
        $project: {
            dish: '$dishes',
            'merchant': '$$CURRENT',
            _id: 0
        }
    }, {
        $project: {
            dish: 1,
            'merchant.location': 1,
            'merchant.info': 1,
            'merchant.name': 1,
            'merchant._id': 1,
            'merchant.address': 1,
            'merchant.contact': 1,
            'merchant.actions': 1
        }
    }, {
        $group: {
            _id: '$dish._id',
            dish: {
                $first: '$dish'
            },
            merchants: {
                $push: '$merchant'
            }
        }
    }, {
        $project: {
            _id: 0,
            dish: 1,
            merchants: 1
        }
    }], function(err, results) {

        if (err) {
            console.log(err)
        } else {
            //console.log("dishes are: %s",results.length)

            results.forEach(function(result) {
                //console.log(result.dish.name)
                var dish_ios_photo
                if (result.dish.photo) {
                    dish_ios_photo = result.dish.photo.ios
                }
                var dish_desc
                if (result.dish.description) {
                    dish_desc = result.dish.description
                }

                var dish_ingre
                if (result.dish.ingredients) {
                    dish_ingre = result.dish.ingredients
                }

                var dish_cuisine
                if (result.dish.cuisine) {
                    dish_cuisine = result.dish.cuisine
                }
                var dish_tags
                if (result.dish.tags) {
                    dish_tags = result.dish.tags
                }
                var query = {
                    index: {
                        _index: "hngre",
                        _type: "dishes",
                        _id: result.dish._id.toString()
                    }
                }

                if (result.dish.approve_status == "true" || result.dish.approve_status == 1) {
                    var merchants = []
                    var count = 0

                    result.merchants.forEach(function(merchant1) {

                        //console.log(merchant1.actions,merchant1._id)
                        // if(merchant1.address.country == "India")
                        // {
                        //     console.log(merchant1)
                        // }
                        merchants.push({
                            name: merchant1.name,
                            _id: merchant1._id,
                            price_range: merchant1.info.price_range,
                            location: {
                                lat: merchant1.location.coordinates[1],
                                lon: merchant1.location.coordinates[0]
                            },
                            phone: merchant1.contact.phone,
                            hours: merchant1.info.hours,
                            meal: merchant1.info.meal,
                            address: {
                                street_address: merchant1.address.street_address, // Thoroughfare
                                neighbourhood: merchant1.address.neighbourhood, // Sublocality
                                locality: merchant1.address.locality, // City (Google)
                                region: merchant1.address.region, // State, Adminstrative area
                                zip: merchant1.address.zip, // Postal Code
                                landmark: merchant1.address.landmark,
                                country: merchant1.address.country
                            },
                            actions: merchant1.actions

                        })

                    })

                    var body = {
                        name: result.dish.name,
                        description: dish_desc,
                        veg_type: result.dish.veg_type,
                        photo: dish_ios_photo,
                        course: result.dish.course,
                        ingredients: dish_ingre,
                        cuisine: dish_cuisine,
                        tags: dish_tags,
                        merchant: merchants
                    }
                    bulk_query.push(query)
                    bulk_query.push(body)
                } else {
                    var query = {
                        delete: {
                            _index: "hngre",
                            _type: "dishes",
                            _id: result.dish._id.toString()
                        }

                    }
                    bulk_query.push(query)
                }
            })
        }

        callback()
    })


}

function locsync(callback) {

    console.log("Start Lsuggest Sync")

    elasticClient.deleteByQuery({
        index: "autocomplete",
        type: "lsuggest",
        body: {
            query: {
                match_all: {}
            }
        }
    }, function(err, res) {
        //console.log(res)
    })

    async.parallel({
        zip: function(callback) {
            console.log("getting Zips")
            merchant.aggregate([{
                $match: {
                    status: 'completed'
                }
            }, {
                $project: {
                    _id: 0,
                    address: 1
                }
            }, {
                $unwind: '$address.neighbourhood'
            }, {
                $group: {
                    _id: {
                        country: '$address.country',
                        region: '$address.region',
                        locality: '$address.locality',
                        zip: '$address.zip'
                    },
                    count: {
                        $sum: 1
                    }
                }
            }, {
                $project: {
                    _id: 0,
                    country: '$_id.country',
                    region: '$_id.region',
                    locality: '$_id.locality',
                    phrase: '$_id.zip',
                    count: 1
                }
            }], function(err, results) {
                if (err) {
                    console.log(err)
                } else {
                    callback(null, results)
                }
            })

        },
        neighbourhood: function(callback) {
            console.log("getting Neighs")

            merchant.aggregate([{
                $match: {
                    status: 'completed'
                }
            }, {
                $project: {
                    _id: 0,
                    address: 1
                }
            }, {
                $unwind: '$address.neighbourhood'
            }, {
                $group: {
                    _id: {
                        country: '$address.country',
                        region: '$address.region',
                        locality: '$address.locality',
                        neighbourhood: '$address.neighbourhood'
                    },
                    count: {
                        $sum: 1
                    }
                }
            }, {
                $project: {
                    _id: 0,
                    country: '$_id.country',
                    region: '$_id.region',
                    locality: '$_id.locality',
                    phrase: '$_id.neighbourhood',
                    count: 1
                }
            }], function(err, results) {
                if (err) {
                    console.log(err)
                } else {

                    callback(null, results)
                }
            })

        },
        locality: function(callback) {
            console.log("getting Localities")
            merchant.aggregate([{
                $match: {
                    status: 'completed'
                }
            }, {
                $project: {
                    _id: 0,
                    address: 1
                }
            }, {
                $group: {
                    _id: {
                        country: '$address.country',
                        region: '$address.region',
                        locality: '$address.locality'
                    },
                    count: {
                        $sum: 1
                    }
                }
            }, {
                $project: {
                    _id: 0,
                    country: '$_id.country',
                    region: '$_id.region',
                    phrase: '$_id.locality',
                    locality: '$_id.locality',
                    count: 1
                }
            }], function(err, results) {
                if (err) {
                    console.log(err)
                } else {
                    //console.log(results)

                    callback(null, results)
                }
            })

        }
    }, function(err, results) {

        if (err) {
            console.log(err)

        } else {

            //console.log(_.keys(results))
            var keys = _.keys(results)

            keys.forEach(function(key) {



                //console.log(results[key].length)

                results[key].forEach(function(data) {



                    var query = {
                        index: {
                            _index: "autocomplete",
                            _type: "lsuggest"
                        }
                    }


                    var body = {
                        phrase: data.phrase,
                        scope: key,
                        locality: data.locality,
                        region: data.region,
                        country: data.country,
                        count: data.count

                    }

                    bulk_query.push(query)
                    bulk_query.push(body)


                })



            })
            callback()

        }



    })

}

function merchantsync(callback) {


    elasticClient.deleteByQuery({
            index: "hngre",
            type: "merchants",
            body: {
                query: {
                    match_all: {}
                }
            }
        }, function(err, res) {

        }) // Delete Query

    var merchants = []
    merchant.find({
            "status": "completed"
        }, function(err, res) {
            if (err) throw err
            else {
                //console.log(res.length)
                res.forEach(function(mer) {
                        //mer = JSON.parse(mer)
                        if (mer.address.country == "United States") {
                            mer._doc.timezone = "America/New_York"

                        }
                        //console.log(mer.timezone)


                        var dishes = []
                        var actions = []
                        var available_veg_type = []
                            // if(mer.actions != undefined)
                            // {
                            //     console.log(mer.actions[0])
                            //     mer.actions.forEach(function(action){
                            //         //console.log(action)
                            //         // actions.push({
                            //         //     url:action.url,
                            //         //     name:action.name,
                            //         //     icon:action.icon,
                            //         //     action:action.action
                            //         // })
                            //     })
                            // }
                            //console.log(actions)
                        if (mer.address.country == "India") {
                            //console.log(mer.actions)

                        }

                        if (mer.actions.length == 0) {
                            console.log(mer.name)
                        }
                        //console.log(mer.name)
                        mer.dishes.forEach(function(dish) {

                            if (dish.approve_status) {
                                var dish_ios_photo
                                if (dish.photo) {
                                    dish_ios_photo = dish.photo.ios
                                }
                                var dish_desc
                                if (dish.description) {
                                    dish_desc = dish.description
                                }

                                var dish_ingre
                                if (dish.ingredients) {
                                    dish_ingre = dish.ingredients
                                }

                                var dish_cuisine
                                if (dish.cuisine) {
                                    dish_cuisine = dish.cuisine
                                }
                                var dish_tags
                                if (dish.tags) {
                                    dish_tags = dish.tags
                                }

                                var dish_source
                                if (dish.source) {
                                    dish_source = dish.source
                                }

                                var dish_cost
                                if (dish.cost) {
                                    dish_cost = {
                                        value: dish.cost.value,
                                        currency: dish.cost.currency
                                    }
                                } else {
                                    dish_cost = {
                                        value: 0,
                                        currency: "₹"
                                    }
                                }



                                var dish_contri = {}
                                    // if (!dish.contributor) {
                                    //     dish_contri = {
                                    //         //_id: dish.contributor._id.toString()
                                    //         _id: "56bd8084e4bb7c670e8018d5",
                                    //         discoveries: 875
                                    //     }
                                    // }
                                    //     if (dish_contri._id == "56bd8084e4bb7c670e8018d5") {                                 

                                //             dish_contri.discoveries= 874
                                //     }
                                //     // if (dish_contri._id == "56bd8053196be24f0e5303d9") {                                   
                                //     //         dish_contri.discoveries = 3098
                                //     // }
                                // }
                                //available_veg_type.push(dish.veg_type)
                                //console.log(dish)
                                // if (!dish.name) {
                                //     console.log(dish)
                                // }

                                dishes.push({
                                    name: dish.name.trim(),
                                    description: dish_desc,
                                    veg_type: dish.veg_type,
                                    photo: dish_ios_photo,
                                    course: dish.course,
                                    ingredients: dish_ingre,
                                    cuisine: dish_cuisine,
                                    tags: dish_tags,
                                    _id: dish._id,
                                    source: dish_source,
                                    cost: dish_cost,
                                    contributor: dish_contri
                                })
                                var dish_type = []
                                dish_type.push(dish.veg_type)
                                    //console.log(dish_type)
                                available_veg_type = _.union(dish_type, available_veg_type)
                            }

                        })


                        var body = {
                            name: mer.name,
                            price_range: mer.info.price_range,
                            location: {
                                lat: mer.location.coordinates[1],
                                lon: mer.location.coordinates[0]
                            },
                            phone: mer.contact.phone,
                            hours: {
                                mon: mer.info.hours.mon,
                                tue: mer.info.hours.tue,
                                wed: mer.info.hours.wed,
                                thu: mer.info.hours.thu,
                                fri: mer.info.hours.fri,
                                sat: mer.info.hours.sat,
                                sun: mer.info.hours.sun,
                            },
                            meal: mer.info.meal,
                            address: {
                                street_address: mer.address.street_address, // Thoroughfare
                                neighbourhood: mer.address.neighbourhood, // Sublocality
                                locality: mer.address.locality, // City (Google)
                                region: mer.address.region, // State, Adminstrative area
                                zip: mer.address.zip, // Postal Code
                                landmark: mer.address.landmark,
                                country: mer.address.country
                            },
                            actions: mer.actions,
                            available_veg_type: available_veg_type,
                            dishes: dishes,
                            timezone: mer._doc.timezone

                        }

                        var query = {
                            index: {
                                _index: "hngre",
                                _type: "merchants",
                                _id: mer._id.toString()
                            }
                        }

                        bulk_query.push(query)
                        bulk_query.push(body)

                    }) // ForEach
                callback()

            } //If else
        }) // Merchant Find

}