/**
 * Created by Partinder on 4/15/15.
 */


var elasticsearch = require("elasticsearch")
var config = require("../config")
var elasticClient = new elasticsearch.Client({
    //log : "trace",
     

    //host: "192.168.1.4:9200"
    //host: "localhost:9200"
    
    //host: '54.84.67.184:9200'
    //host: '52.91.167.165:9200'
    host: config.elasticsearch


})
var elasticFormat = require("./helpers").elasticFormat


module.exports = {
    createIndex : function(merchant){

        if(merchant.dishes.length != 0)
        {
            var format_dishes =[]
            format_dishes = elasticFormat(merchant)
            var dishes = format_dishes[0]
            var dishes_del = format_dishes[1]

            dishes.forEach(function(dish){

                //console.log(dish.name)
                elasticClient.index(dish,function(err,res){
                    if(err)
                    {
                        console.log(err)
                    }
                    else
                    {
                        console.log("Update Dish: %s in Elastic",dish.body.name)
                    }

                })

            })
            if(dishes_del.length != 0){
                dishes_del.forEach(function(dishid){
                    elasticClient.delete({
                        index:"hngre",
                        type:"dishes",
                        id:dishid
                    },function(err,res){
                        if(err)
                        {
                            console.log(err)
                        }
                        else
                        {
                            console.log("Dish Deleted, Id(s): %s",dishid)
                        }
                    })

                })

            }
        }

        elasticClient.count(function(err,res,status){
            console.log("Total Entires in Elastic %s", res.count)

        })

        //elasticClient.count(function(err,res,status){
        //    console.log(res.count)
        //})

        //elasticClient.search({
        //    q: ''
        //}).then(function (body) {
        //    var hits = body.hits.hits;
        //    console.log(hits)
        //}, function (error) {
        //    console.trace(error.message);
        //});





    },
    updateIndex : function(merchant){



    },
    deleteIndex: function(merchant){


    },
    bulkOperation: function(bulk_query,callback){
        elasticClient.bulk({
            body:bulk_query
        },
        function(err,res){
            if(err)
            {
                console.log(err)
                callback(err)

            }
            else
            {
                console.log("Elastic Sync Complete")
                callback(err)
            }
        })
    }
}

