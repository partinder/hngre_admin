var mongoClient = require("mongodb").MongoClient

mongoClient.connect("mongodb://localhost/db_hngre", function(err, db) {
    if (err) {
    	console.log(err)

    } else {
        var merchants = db.collection("merchants")

        merchants.find({
            "status": "completed"
        }).toArray(function(err, res) {
            if (err) throw err
            else {                
                asyncloop(res.length, function(loop1) {
                	mer = res[loop1.iteration()]
                	//console.log(mer)

                	mer.dishes.forEach(function(dish){
                		dish.source ={
                			name: mer.name,
                			link:null
                		}
                	})
                	//console.log(mer)
                	merchants.update({"_id":mer._id},mer,function(err,count,status){
                		if(err) console.log(err)
                			else
                			{
                				console.log(status)
                				loop1.next()
                			}
                				
                	})
                	
                   
                   
                }, function() {
                    console.log("All DONE")

                })

            }
        })

    }
})

function asyncloop(iterations, func, callback) {
    var index = 0;
    var done = false;
    var loop = {
        next: function() {
            if (done) {
                return;
            }

            if (index < iterations) {
                index++;
                func(loop);

            } else {
                done = true;
                callback();
            }
        },

        iteration: function() {
            return index - 1;
        },

        break: function() {
            done = true;
            callback();
        }
    };
    loop.next();
    return loop;
}