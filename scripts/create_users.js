/**
 * Created by Partinder on 5/11/15.
 */

var MongoClient = require("mongodb").MongoClient
var ObjectID = require('mongodb').ObjectID


MongoClient.connect('mongodb://localhost/db_hngre', function (err, db) {


    if (err) {
        throw err
    }
    else {

        var result = []
        var reviews = db.collection("reviews")


        asyncLoop(8,function(loop){

            reviews.aggregate([
                {$group: {_id: "$reviewer_id", "name": {$first: "$reviewer_name"}, "total_review_count":{$max: "$reviewer_count"}, photo: {$first: "$reviewer_photo"}, location:{$first: "$reviewer_location"}, reviews: {$addToSet: {review: "$review", rating: "$review_rating", rest_yelp: "$yelp_link"}}, int_count:{$sum: 1}}},
                {$match: {"int_count": {$gte: 1 }}},
                {$skip: loop.iteration()*1000},
                {$limit: 1000}
                //{$group: {_id: null, total: {$sum: 1}}}
            ],function(err,result_agre){
                if(err)
                {
                    console.log(err)
                }
                else
                {
                    console.log("Loop Number: %s",loop.iteration())
                    console.log("Aggregrate result count: %s",result_agre.length)

                    result_agre.forEach(function(user){
                        //console.log(user.name)
                        result.push(user)
                    })
                    console.log("Result Length: %s", result.length)
                    loop.next()

                }
            })





        },function(){

            console.log(result.length)
            console.log("Dropping old users")

            db.dropCollection("dumy_users", function(err,res){
                if(err)
                {
                    console.log(err)
                }
                else
                {
                    var dumy_users = db.collection("dumy_users")

                    result.forEach(function(user){
                        user.status = 0
                        user.yelpid = user._id
                        user._id = new ObjectID
                        dumy_users.insert(user,function(err,sta){
                            if(err)
                            {
                                console.log(err)
                            }
                            else
                            {
                                console.log("Created User %s",user.name)
                            }

                        })
                    })

                }
            })
        })


    }
})


function asyncLoop (iterations, func, callback) {
    var index = 0;
    var done = false;
    var loop = {
        next: function() {
            if (done) {
                return;
            }

            if (index < iterations) {
                index++;
                func(loop);

            } else {
                done = true;
                callback();
            }
        },

        iteration: function() {
            return index - 1;
        },

        break: function() {
            done = true;
            callback();
        }
    };
    loop.next();
    return loop;
}