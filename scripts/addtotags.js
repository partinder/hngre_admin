/**
 * Created by Partinder on 8/6/15.
 */

var mongoClient = require("mongodb").MongoClient
var ObjectId = require("mongodb").ObjectID
var natural = require("natural")
var tokenizer = new natural.WordTokenizer();
var fs = require("fs")
var regex = require("regex")

var stop = fs.readFileSync("../stop.txt").toString().split("\n")


mongoClient.connect("mongodb://localhost/db_hngre",function(err,db){
    if(err) throw err
    else
    {
        if (typeof Array.prototype.reIndexOf === 'undefined') {
            Array.prototype.reIndexOf = function (rx) {
                for (var i in this) {
                    if (this[i].toString().match(rx)) {
                        return i;
                    }
                }
                return -1;
            };
        }
        console.log("Connected")
        var merchants = db.collection("merchants")
        merchants.find({"status":"completed","address.country":"India"}).sort({"yelp.review_count": -1 }).toArray(function(err,result){
            asyncloop(result.length,function(loop){
                var merchant = result[loop.iteration()]
                for(var d=0;d<merchant.dishes.length;d++)
                {
                    var dish = merchant.dishes[d]
                    var termsToAdd = []
                    var dishTerm = tokenizer.tokenize(dish.name)

                    for(i=0;i<dishTerm.length;i++)
                    {
                        var term = new RegExp(dishTerm[i], 'i')

                        if(stop.reIndexOf(term) == -1)
                        {
                            if(merchant.dishes[d].tags.reIndexOf(term) == -1)
                            {
                                merchant.dishes[d].tags.push(dishTerm[i])

                            } 

                        }

                    }

                }
                // Save Updated Merchant
                console.log("Updating Mechant",merchant.name,merchant._id )
                merchants.update({"_id":ObjectId(merchant._id)},{$set:{dishes:merchant.dishes}},function(err,res){
                    if(err) throw err
                    else console.log(res)
                    loop.next()
                })


            },function(){
                console.log("All Done")
            })
        })
    }
})


function asyncloop (iterations, func, callback) {
    var index = 0;
    var done = false;
    var loop = {
        next: function() {
            if (done) {
                return;
            }

            if (index < iterations) {
                index++;
                func(loop);

            } else {
                done = true;
                callback();
            }
        },

        iteration: function() {
            return index - 1;
        },

        break: function() {
            done = true;
            callback();
        }
    };
    loop.next();
    return loop;
}


