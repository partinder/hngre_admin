(function() {
    'use strict';
    var mongojs = require('mongojs');
    var async = require('async');
    var _ = require('underscore');
    var config = require("../config")

    var nodemailer = require('nodemailer');
    var mailgun = require('nodemailer-mailgun-transport');

    var transporter = nodemailer.createTransport(mailgun({
        auth: {
            api_key: 'key-a184af3ea4a06a679240732867d0435d',
            domain: 'hngremail.com'
        }
    }));
    //production db: 52.74.138.138
    var db = mongojs('mongodb://'+config.db+'/db_hngre', ['users', 'interactions']);
    db.on('error', function(error) {
        console.log("database error", error);
    });
    db.on('connect', function() {
        console.log("database connected");
    });
    var yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    var today = new Date();
    today = today.toISOString();
    yesterday = yesterday.toISOString();
    console.log(today, yesterday);
    db.interactions.distinct('_user', {
        timestamp: {
            $gte: new Date(yesterday),
            $lt: new Date(today)
        }
    }, function(err, userIds) {
        if (err) {
            console.log(err);
            return;
        }
        console.log("Total Users", userIds.length);
        async.parallel(userIds.map(function(userId) {
            return function(callback) {
                db.users.findOne({
                    _id: userId
                }, {
                    name: 1,
                    first_name: 1,
                    last_name: 1,
                    email: 1,
                    mixpanel_id: 1,
                    devices: 1
                }, function(err, user) {
                    if (err) {
                        return callback(err);
                    }
                    if (!user) return callback(null, null);
                    return callback(null, user);
                });
            };
        }), function(err, users) {
            if (err) {
                console.log(err);
                return;
            }
            users = users.filter(function(user) {
                if (user) return true;
                else return false;
            });
            if (users && users.length && users.length > 0) {
                var iosCount = 0;
                var androidCount = 0;
                async.parallel(users.map(function(user) {
                    return function(callback) {
                        db.interactions.find({
                            _user: user._id,
                            timestamp: {
                                $gte: new Date(yesterday),
                                $lt: new Date(today)
                            }
                        }).sort({
                            timestamp: -1
                        }, function(err, interactions) {
                            if (err) {
                                return callback(err);
                            }
                            if (interactions && interactions.length && interactions.length > 0) {
                                var devices = user.devices;
                                var type = "";
                                if (devices && devices.length > 0) {
                                    devices = devices[0].agent.os;
                                    if (devices && devices.family && devices.family.toLowerCase() == "ios") {
                                        iosCount++;
                                        type = "iOS";
                                    } else {
                                        androidCount++;
                                        type = "Android";
                                    }
                                }
                                return callback(null, {
                                    user: user._id,
                                    name: user.name,
                                    email: user.email,
                                    mixpanel_id: user.mixpanel_id,
                                    interaction_count: interactions.length,
                                    start_interaction: interactions[0].timestamp,
                                    end_interaction: interactions[interactions.length - 1].timestamp,
                                    device: type
                                });
                            }
                        });
                    };
                }), function(err, data) {
                    if (err) {
                        console.log(err);
                        return;
                    }
                    data = "iOS users : " + iosCount + "\nAndroid users : " + androidCount + "\n\n" + JSON.stringify(data, null, ' ');
                    var mailOptions = {
                        from: 'info@hngremail.com', // sender address
                        to: 'suresh.mahawar@hngre.com, partinder@hngre.com, rohit@hngre.com', // list of receivers
                        subject: 'Today app users count ' + users.length, // Subject line
                        text: data
                    };
                    transporter.sendMail(mailOptions, function(err, res) {
                        if (err) {
                            console.log(err);
                            return;
                        }
                        console.log(res);
                    });
                });
            } else {
                console.log("Ops!! No Users interacted!!");
                var mailOptions = {
                    from: 'info@hngremail.com', // sender address
                    to: 'suresh.mahawar@hngre.com, partinder@hngre.com, rohit@hngre.com', // list of receivers
                    subject: 'Today app users count 0', // Subject line
                    text: "Ops!! No Users interacted!!"
                };
                transporter.sendMail(mailOptions, function(err, res) {
                    if (err) {
                        console.log(err);
                        return;
                    }
                    console.log(res);
                });
            }
        });
    });
}());