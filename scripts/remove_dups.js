/**
 * Created by Partinder on 6/12/15.
 */

var mongoClient = require("mongodb").MongoClient
var merchant = require("../models/merchant").merchant
var mongoose = require("mongoose")
//var Regex = require("regex")


mongoose.connect("mongodb://localhost/db_hngre",function(err,res){
    if(err) throw err
})


var fs = require('fs');
var dups = []
var array = fs.readFileSync('../dups.txt').toString().split("\n");
for(i in array)
{
    dups.push(array[i])

}

asyncloop(dups.length,function(loop){

    // Remove Later : shrimp[,shrimp
    var dup = dups[loop.iteration()].split(",")
    //console.log(dup)
    //var regex = new RegExp(dup[0],"i")
    merchant.find({"dishes.ingredients":dup[0]},function(err,res){
        //console.log(loop.iteration())
        if(err)
        {
            throw  err
        }
        else
        {
            asyncloop(res.length,function(loop1){
                var mer = res[loop1.iteration()]
                //console.log(loop1)

                var dishes_length = mer.dishes.length

                asyncloop(dishes_length,function(loop2){

                    var ingredient_length = mer.dishes[loop2.iteration()].ingredients.length

                    if(mer.dishes[loop2.iteration()].ingredients.indexOf(dup[0]) != -1)
                    {

                        var index = mer.dishes[loop2.iteration()].ingredients.indexOf(dup[0])
                        console.log(mer.dishes[loop2.iteration()].ingredients[index])
                        mer.dishes[loop2.iteration()].ingredients[index] = dup[1]
                        console.log(mer.dishes[loop2.iteration()].ingredients[index])

                        mer.markModified('dishes.'+ loop2.iteration() +'.ingredients')

                        mer.save(function(err){
                            if(err)
                            {
                                throw err

                            }
                            else
                            {
                                loop2.next()
                                console.log("saved")

                            }
                        })
                    }
                    else
                    {
                        loop2.next()
                    }


                },function(){
                    loop1.next()
                })




                //asyncloop(res[loop1.iteration()].dishes.length,function(loop2){
                //    //console.log(res.length,dup[0],res[loop1.iteration()].dishes.length)
                //    if(res[loop1.iteration()].dishes[loop2.iteration()].ingredients.indexOf(dup[0]) != -1)
                //    {
                //        console.log(dup)
                //        //console.log(res[loop1.iteration()].dishes[loop2.iteration()].ingredients.indexOf(dup[0]))
                //        var mer = res[loop1.iteration()]
                //            var temp =mer.dishes[loop2.iteration()].ingredients[mer.dishes[loop2.iteration()].ingredients.indexOf(dup[0])]
                //        var index = mer.dishes[loop2.iteration()].ingredients.indexOf(dup[0])
                //        console.log(mer.dishes[loop2.iteration()].ingredients[index])
                //        mer.dishes[loop2.iteration()].ingredients[mer.dishes[index] = dup[1]
                //        console.log(mer.dishes[loop2.iteration()].ingredients[index])
                //
                //        mer.save(function(err){
                //                console.log("Saved")
                //                if(err)
                //                {
                //                    throw err
                //                }
                //                loop2.next()
                //            })
                //        //loop2.next()
                //
                //    }
                //    else
                //    {
                //        loop2.next()
                //
                //    }
                //
                //
                //
                //},function(){
                //    loop1.next()
                //})

            },function(){
                loop.next()
            })


        }

    })

},function(){
    console.log("All Done")

})


function asyncloop (iterations, func, callback) {
    var index = 0;
    var done = false;
    var loop = {
        next: function() {
            if (done) {
                return;
            }

            if (index < iterations) {
                index++;
                func(loop);

            } else {
                done = true;
                callback();
            }
        },

        iteration: function() {
            return index - 1;
        },

        break: function() {
            done = true;
            callback();
        }
    };
    loop.next();
    return loop;
}