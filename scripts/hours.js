/**
 * Created by Partinder on 10/19/15.
 */

var mongoClient = require("mongodb").MongoClient
var clientId = 'H2CQMGMWQN415DQDMGXN1ZWWSO4BBABEKMDTH1K4UZYQK3NU';
var clientSecret = 'YFUACG3ZHWCE5FUC4AWNNNKSIBYCQPGWNYCACSG2FM3F1ASF';

var foursquare = require('node-foursquare-venues')(clientId, clientSecret);

var daysArray = [
    'mon',
    'tue',
    'wed',
    'thu',
    'fri',
    'sat',
    'sun'
];
module.exports = function(callback){
    mongoClient.connect("mongodb://localhost/db_hngre",function(err,db){
        if(err)
        {
            throw err
        }
        else
        {
            console.log("Connected to DB")
            var merchant = db.collection("merchants")
            merchant.find({"foursquare":{$exists:true}}).toArray(function(err,result){
                if(err) throw err
                else
                {
                    console.log(result.length)
                    asyncloop(result.length,function(loop)
                    {
                        getHours(result[loop.iteration()].foursquare.id,function(err,res){
                            if(err){
                                merchant.update({"_id":result[loop.iteration()]._id},{$set:{"info.hours":null}},function(err,count,status){
                                    if(err){
                                        console.log(err)
                                        setTimeout(function(){
                                            loop.next()
                                        },500)
                                    }
                                    else
                                    {
                                        console.log(status, result[loop.iteration()].name)
                                        setTimeout(function(){
                                            loop.next()
                                        },500)
                                    }
                                })
                            }
                            else
                            {
                                //console.log(res)
                                merchant.update({"_id":result[loop.iteration()]._id},{$set:{"info.hours":res}},function(err,count,status){
                                    if(err){
                                        console.log(err)
                                        setTimeout(function(){
                                            loop.next()
                                        },500)
                                    }
                                    else
                                    {
                                        console.log(status, result[loop.iteration()].name)
                                        setTimeout(function(){
                                            loop.next()
                                        },500)
                                    }
                                })
                            }
                        })

                    },function(){
                        console.log("All Done")
                        callback("All Done")
                    })
                }
            })
        }
    })

}

function asyncloop (iterations, func, callback) {
    var index = 0;
    var done = false;
    var loop = {
        next: function() {
            if (done) {
                return;
            }

            if (index < iterations) {
                index++;
                func(loop);

            } else {
                done = true;
                callback();
            }
        },

        iteration: function() {
            return index - 1;
        },

        break: function() {
            done = true;
            callback();
        }
    };
    loop.next();
    return loop;
}
/*
 FOURSQUARE-CONNECT
 Relevant methods for connecting with the Foursquare API.
 */


function transformFoursquareHours (foursquareHours)
{
    var transformed = {};

    daysArray.forEach(function(day) {
        transformed[day] = [];
    });

    foursquareHours.forEach(function (timeFrame) {

        timeFrame.days.forEach(function (day) {

            var openSlots = timeFrame.open;

            var slots = [];

            openSlots.forEach(function (openSlot) {

                slots.push([parseInt(openSlot.start), parseInt(openSlot.end)]);
            });

            transformed[daysArray[day-1]] = slots;
        });
    });

    return transformed;
}

function getHours (venueId, callback) {

    foursquare.venues.hours(venueId, function (err, result) {

        if (err) return callback(err);

        //Transform hours into readable format
        if (result.meta.code === 200)
        {
            if (result.response.hours.timeframes)
            {
                callback (null, transformFoursquareHours(result.response.hours.timeframes));
            }
            else
            {
                callback(new Error('No hours data available'));
            }
        }
        else
        {
            callback(new Error('Problem getting hours'))
        }
    });
}
