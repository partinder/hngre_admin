/*
 DB Transform
 Transformation of document formats as stored in the MongoDB data store.
 */
"use strict";

var MongoClient = require('mongodb').MongoClient;
var Mongoose = require('mongoose');
var newMerchant = require('./../models/merchant').merchantschema
//Model representing old merchant

var connection = Mongoose.createConnection('mongodb://localhost/db_hngre');
var Merchant_new = connection.model('Merchant', newMerchant);

MongoClient.connect('mongodb://localhost/hngre_copy', function (err, db) {

    if (err) {

        console.log('ERROR Connecting to old database: ', err);

    } else {

        console.log('Successfully connected to hngre_copy');

        var merchant_old = db.collection('merchants');
        var parseCount = 0;
        var saveCount = 0;
        var startTime = Date.now();
        var readEnded = false;

        var merchantStream = merchant_old.find().stream();

        merchantStream.on('error', function (err) {
            console.log('ERROR creating merchant stream:', err);
        });

        merchantStream.on('end', function(err) {
            console.log('Reading ended');
            readEnded = true;
        });

        merchantStream.on('data', function (item) {
            //console.log(item);
            parseCount++;

            var new_item = new Merchant_new();

            function checkString(key_name, key_value){
                if(key_value){

                    new_item[key_name] = key_value;
                }
            }

            function checkSubString (key_name, key_value){
                //console.log(key_value)
                if(key_value)
                {
                    var arr = key_name.split(".");
                    var end = new_item;

                    new_item[arr[0]][arr[1]] = key_value;
                    //console.log(new_item)
                    //console.log(new_item)
                }
            }

            function checkArray(key_name, key_value){
                if(key_value){

                    new_item[key_name].push(key_value);
                    //console.log(new_item)

                }
            }
            function checkSubArray(key_name, key_value){
                if(key_value){
                    var arr = key_name.split('.');

                    new_item[arr[0]][arr[1]].push(key_value);
                    //console.log(new_item.coordinates)

                }
            }

            function checkCsv (key_name, key_value) {

                if (key_value) {

                    new_item[key_name] = key_value.split(",");
                }

            }

            function checkSubCsv (key_name, key_value) {

                if (key_value) {

                    var arr = key_name.split('.');
                    var arr1 = key_value.split(",");
                    for (var i=0; i< arr1.length; i++) {
                        arr1[i]= arr1[i].trim()
                    }
                    new_item[arr[0]][arr[1]] = arr1;


                    //console.log(arr1)
                }

            }

            function checkBool(key_name, key_value){
                if(key_value)
                {
                    var arr = key_name.split('.');

                    if(key_value === "Yes") {
                        new_item[arr[0]][arr[1]] = 1;
                    } else {
                        new_item[arr[0]][arr[1]] = 0;
                    }
                    //console.log(new_item.info)
                }
            }

            function checkCoordinates(key_name, lon,lat){
                if(lon)
                {
                    new_item.location = {
                        type: "Point",
                      coordinates: [lon,lat]
                    }
                }
                //console.log(new_item.coordinates)
            }

            checkString     ("_id", item._id);
            checkString     ("name", item.merchant_name);
            checkSubString  ("yelp.link",item.merchant_yelp_link);
            checkSubString  ("yelp.rating",parseFloat(item.merchant_yelp_rating));
            checkSubString  ("yelp.review_count",parseInt(item.merchant_yelp_review_count));
            checkSubString  ("contact.primary_email",item.merchant_email);
            checkSubString  ("address.street_address",item.merchant_street_address);
            checkSubString  ("address.locality",item.merchant_locality);
            checkSubString  ("address.region",item.merchant_region);
            checkSubString  ("address.zip",item.merchant_zip);
            checkSubString  ("address.landmark",item.merchant_street_address_landmark);
            checkSubString  ("info.price_range",item.merchant_price_range);
            checkSubString  ("info.attire",item.merchant_attire);
            checkSubString  ("info.noise",item.merchant_noise_level);
            checkSubString  ("info.dancing",item.merchant_dancing);

            checkSubString  ("info.alcohol",item.merchant_alcohol);
            checkSubString  ("info.music",item.merchant_music);
            checkSubString  ("zagat.food",parseInt(item.merchant_zagat_rating_food));
            checkSubString  ("zagat.decor",parseInt(item.merchant_zagat_rating_decor));
            checkSubString  ("zagat.service",parseInt(item.merchant_zagat_rating_service));
            checkSubString  ("social.facebook",item.merchant_facebook);
            checkSubString  ("social.twitter",item.merchant_twitter);
            checkString     ("status",item.merchant_status);
            checkString     ("photo",item.merchant_photo);
            checkString     ("source",item.merchant_source);
            checkString     ("hngre_url",item.merchant_unique_url);
            checkString     ("url",item.merchant_weburl);

            checkArray("cuisine",item.merchant_cuisine1);
            checkArray("cuisine",item.merchant_cuisine2);
            checkArray("cuisine",item.merchant_cuisine3);
            checkSubArray("contact.email",item.merchant_old_email);
            checkSubArray("contact.phone",item.merchant_tel);
            checkSubArray("address.neighbourhood",item.merchant_neighborhood1);
            checkSubArray("address.neighbourhood",item.merchant_neighborhood2);
            checkSubArray("address.neighbourhood",item.merchant_neighborhood3);
            checkArray("transit",item.merchant_transit1);
            checkArray("transit",item.merchant_transit2);
            checkArray("transit",item.merchant_transit3);
            checkCoordinates("location",parseFloat(item.merchant_longtitude),parseFloat(item.merchant_latitude));

            //checkArray("coordinates",item.merchant_latitude)

            checkSubCsv("info.meal", item.merchant_good_for_meal);
            checkSubCsv("info.ambience",item.merchant_ambience);
            checkCsv("tags",item.merchant_tags);
            checkCsv("influencers",item.merchant_influencers);
            checkSubCsv("info.best_nights",item.merchant_best_nights);
            checkSubCsv("info.merchant_type",item.merchant_type);
            checkSubCsv("info.parking",item.merchant_parking);

            checkBool("info.credit_card",item.merchant_credit_card_accept_status);
            checkBool("info.groups",item.merchant_good_for_groups);
            checkBool("info.kids",item.merchant_good_for_kids);
            checkBool("info.reservations",item.merchant_takes_reservations);
            checkBool("info.delivery",item.merchant_delivery);
            checkBool("info.takeout",item.merchant_takeout);
            checkBool("info.table_service",item.merchant_table_service);
            checkBool("info.outdoor_seating",item.merchant_outdoor_seating);
            checkBool("info.wifi",item.merchant_wifi);
            checkBool("info.alcohol_byo",item.merchant_alcohol_byo);
            checkBool("info.smoking",item.merchant_smoking);
            checkBool("info.coat",item.merchant_coat_check);
            checkBool("info.tv",item.merchant_tv);
            checkBool("info.caters",item.merchant_caters);
            checkBool("info.wheelchair", item.merchant_wheelchair);

            if(item.merchant_hours)
            {
                new_item.info.hours = getTimeSheetFromString(item.merchant_hours, item._id);
            }

            //console.log('Created new item. Saving...');

            new_item.save(function(err){
                if(err)
                {
                    throw err;
                }
                //console.log(JSON.stringify(new_item));
                console.log('Saved', ++saveCount);

                if (readEnded && (saveCount === parseCount)) {
                    console.log('Items saved:', saveCount);
                    var timeTaken = Date.now() - startTime;
                    console.log('Time Taken:', timeTaken, 'ms');

                    console.log('Total time sheet errors:', num_errors);
                    console.log('IDs in which timeSheet errors were found:', error_ids);
                    process.exit();
                }
            });
        });
    }
});


//TIME TRANSFORMER FUNCTIONS

var weekdays_name = ["mon","tue","wed","thu","fri","sat","sun"];
var num_errors = 0;
var error_ids = [];

function getTimeSheetFromString (timeString, merchantId) {

    var evalString = timeString.replace(/ /g, '');

    var matches = [], found;

    var regex = /closed|open24hours|([A-Z][A-Z][A-Z]|(0?[1-9]|1[012]):[0-5][0-9][ap][m])-([A-Z][A-Z][A-Z]|(0?[1-9]|1[012]):[0-5][0-9][ap][m])|[A-Z][A-Z][A-Z]|([A-Z][A-Z][A-Z]|(0?[1-9]|1[012]):[0-5][0-9][ap][m])([A-Z][A-Z][A-Z]|(0?[1-9]|1[012]):[0-5][0-9][ap][m])/ig;

    while (found = regex.exec(evalString)) {
        matches.push(found[0]);
    }

    var timeSheet = {};

    weekdays_name.forEach(function (weekday) {
        timeSheet[weekday] = []
    });

    var currentKey = '';

    matches.forEach(function (match) {

        try {
            match = match.toLowerCase();

            if (weekdays_name.indexOf(match) > -1) {
                currentKey = match;
            } else {
                var timeSlot = getTimeSlotForString(match, currentKey);

                if (timeSlot !== null) {
                    timeSheet[currentKey].push(timeSlot);
                } else {
                    timeSheet[currentKey] = timeSlot;
                }

            }
        } catch (err) {
            console.log('ERROR while parsing:', timeString, err);
            num_errors ++;
            if (error_ids.indexOf(merchantId) < 0) {
                error_ids.push(merchantId);
            }
        }

    });

    return timeSheet;
}

function getTimeSlotForString(str) {

    str = str.toLowerCase();

    if (str === 'closed') {
        return "Closed";
    } else if (str === 'open24hours') {
        return [0,2359];
    } else {

        var times = [];
        var foundTime;

        var regex = /[A-Z][A-Z][A-Z]|(0?[1-9]|1[012]):[0-5][0-9][ap][m]/ig;

        while (foundTime = regex.exec(str)) {
            times.push(foundTime[0]);
        }

        var timeSlot = [];

        times.forEach(function (time) {
            var timeNumber = getTimeNumber(time);

            if (isNaN(timeNumber)) {
                if (weekdays_name.indexOf(time) > -1) {
                    if (timeSlot.length === 0) {
                        timeNumber = 0;
                    } else {
                        timeNumber = 2359;
                    }
                } else {
                    throw new Error('ERROR parsing string '+ str);
                }
            }

            timeSlot.push(timeNumber);
        });

        return timeSlot;
    }
}

function getTimeNumber(time) {

    var old_time = time;

    var new_time = old_time.substring(0, old_time.length - 2);
    var am_pm= old_time.substring(old_time.length - 2, old_time.length);
    //console.log(new_time)
    new_time = new_time.replace(/:/g,"");

    new_time = parseInt(new_time);

    if(am_pm == "am")
    {
        if(new_time > 1200 || new_time == 1200)
        {
            new_time = new_time - 1200;
        }
    }
    else
    {

        if(new_time > 1200 || new_time == 1200)
        {
            new_time = new_time - 1200;
        }
        new_time += 1200;
    }

    return new_time;

}