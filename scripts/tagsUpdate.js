var mongoClient = require("mongodb").MongoClient
var fs = require("fs")
var data = fs.readFileSync("tag-errors.csv", "utf-8").split("\n")
var final = []

data.forEach(function(dat1) {
    dat1 = dat1.split("\r")
    dat1.forEach(function(dat2) {
        final.push(dat2.split(","))
    })

})
//console.log(final)


mongoClient.connect("mongodb://localhost/db_hngre", function(err, db) {
    if (err) {

    } else {
        var merchants = db.collection("merchants")

        merchants.find({
            "status": "completed"
        }).toArray(function(err, res) {
            if (err) throw err
            else {
                console.log(res.length)
                console.log(final.length)
                asyncloop(res.length, function(loop1) {
                    var merchant = res[loop1.iteration()]
                    var flag = 0
                        //console.log(merchant)
                    for (i = 0; i < merchant.dishes.length; i++) {
                        for (y = 0; y < merchant.dishes[i].tags.length; y++) {
                            var tagOld = merchant.dishes[i].tags[y]
                                //console.log(tagOld)

                            for (f = 0; f < final.length; f++) {
                                //console.log(final[f][0])
                                if (merchant.dishes[i].tags[y] == final[f][0]) {
                                    flag = 1
                                    

                                    merchant.dishes[i].tags[y] = final[f][1]
                                    
                                }
                            }
                        }
                    }
                    if (flag == 1) {

                        merchants.update({
                            "_id": merchant._id
                        }, merchant, function(err, count, res) {
                            if (err) {
                                console.log(err)
                            }
                            else
                            {
                                console.log("Updated")
                                loop1.next()
                            }
                        })

                    }
                    else
                    {
                        console.log("No Update")
                        loop1.next()
                    }
                   
                }, function() {
                    console.log("All DONE")

                })

            }
        })

    }
})

function asyncloop(iterations, func, callback) {
    var index = 0;
    var done = false;
    var loop = {
        next: function() {
            if (done) {
                return;
            }

            if (index < iterations) {
                index++;
                func(loop);

            } else {
                done = true;
                callback();
            }
        },

        iteration: function() {
            return index - 1;
        },

        break: function() {
            done = true;
            callback();
        }
    };
    loop.next();
    return loop;
}