var mongoClient = require("mongodb").MongoClient
var access_token = "2157318309.1677ed0.783614b606234931a77573ab87d89c4a"

var request = require("request")
var result = []

var user = {
    _id: '37225080',
    count: 847
}

// request("https://api.instagram.com/v1/users/"+user._id+"/?access_token="+access_token,function(err,res,body){
//                  if(err)
//                  {
//                      console.log(err)
//                      //loop.next()
//                  }
//                  else
//                  {
//                      // res = JSON.parse(res)
//                      body = JSON.parse(res.body)
//                         console.log(body)
//                      //console.log(body.data)

//                  }
//              })

mongoClient.connect("mongodb://localhost/db_hngreV2", function(err, db) {
    if (err) {
        throw err
    } else {

        console.log("Connected to DB HngreV2")
        var instaPics = db.collection("merchantPics")
        var instaUsers = db.collection("instaUsersWEB")
        var users = db.collection("users")
        users.find().toArray(function(err, result) {
            if (err) throw err
            else {
                console.log("Found Users")
                save(result, instaUsers)
            }
        })

        // aggregrate(0,5,instaPics,function(){
        //     console.log("Done 1")
        // aggregrate(10,1000000,instaPics,function(){
        // console.log("Done 2")
        // save(result,instaUsers)
        // aggregrate(15,50,instaPics,function(){
        //     console.log("Done 3")
        //     aggregrate(50,5000000,instaPics,function(){
        //         console.log("All done")
        //         save(result,instaUsers)
        //     })
        // })
        // })
        // })

    }
})

function save(result, instaUsers) {
    console.log(result.length)
    asyncloop(result.length, function(loop) {
        var user = result[loop.iteration()]
            //console.log(user)
        var url = "https://api.instagram.com/v1/users/" + user.user.id + "/?access_token=" + access_token
        request(url, function(err, res, body) {
            if (err) {
                console.log(err)
                setTimeout(function() {
                    loop.next()
                }, 1500)

            } else {
                //console.log(res)
                if (res.statusCode == 200 || res.statusCode== 201) {
                    body = JSON.parse(res.body)
                    instaUsers.insert(body.data, {
                        w: 1
                    }, function(err, res) {
                        if (err) {
                            console.log(err)
                            setTimeout(function() {
                                loop.next()
                            }, 1500)

                        } else {
                            console.log("Created User : %s", body.data.username)
                            setTimeout(function() {
                                loop.next()
                            }, 1500)

                        }
                    })

                }
                else
                {
                    console.log(res.statusCode,user.user.username)
                    setTimeout(function() {
                                loop.next()
                            }, 1500)

                }
                // res = JSON.parse(res)

                //console.log(body.data)

            }
        })

    }, function() {
        console.log("All Done")
    })

}

function aggregrate(from, to, instaPics, callback) {
    instaPics.aggregate([{
        $project: {
            user: true
        }
    }, {
        $group: {
            _id: "$user.id",
            // username: {
            //     $last: "$user.username"
            // },
            // full_name: {
            //     $last: "$user.full_name"
            // },
            count: {
                $sum: 1
            }
        }
    }, {
        $match: {
            count: {
                $gte: from,
                $lt: to
            }
        }
    }, {
        $sort: {
            count: -1
        }
    }], function(err, result1) {
        if (err) throw err
        else {
            result1.forEach(function(res) {
                result.push(res)
            })
            callback()

        }
    })
}




function asyncloop(iterations, func, callback) {
    var index = 0;
    var done = false;
    var loop = {
        next: function() {
            if (done) {
                return;
            }

            if (index < iterations) {
                index++;
                func(loop);

            } else {
                done = true;
                callback();
            }
        },

        iteration: function() {
            return index - 1;
        },

        break: function() {
            done = true;
            callback();
        }
    };
    loop.next();
    return loop;
}