/**
 * Created by Partinder on 1/30/15.
 */
var Mongoose = require("mongoose")
var merchant = require("../models/merchant.js").merchant

Mongoose.connect("mongodb://localhost/db_hngre", function(err){
    if(err)
    {
        logger.error(err)
    }
})

merchant.find({},function(err,merchants){


    merchants.forEach(function(merchant){
        merchant.address.country = "United States"

        merchant.save(function(err){
            if(err)
            {
                console.log(err)
            }
            else
            {
             console.log("Updated Merchant: %s", merchant.name)
            }
        })
    })
})
