var express = require('express');
var router = express.Router();
var authentication = require("../methods/authentication.js")


/* GET home page. */
router.post('/', authentication.ensureAuthenticated, function(req, res) {
  //res.render('index', { title: 'Express' });

  res.send("Routing working fine")
})
.get("/", authentication.ensureAuthenticated,function(req,res){

      res.send("Hello")
    })

module.exports = router;
