/**
 * Created by Partinder on 2/23/15.
 */

var express = require('express');
var router = express.Router();
var authentication = require("../methods/authentication.js")


router.get("/admin/user/dashboard", authentication.ensureAuthenticated, function (req, res) {
    res.render("dashboard_user",{user : req.user, number : 25})
})

router.post("/admin/user/dashboard", function (req, res) {
    dashboard_user.post_handle(req, res)

})