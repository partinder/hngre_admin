var isResetPart2 = false,
    isResetPart3 = false;
$(function() {
	$('#fullpage').fullpage({
		sectionsColor: ['']
	});
	
	$('#fp-nav ul li').each(function(i) {
		$(this).attr('id', 'page'+(i+1));
	});
	$(window).load(animatePart1);
});


function triggetAnimations(value) {
   
    switch (value) {
        case 'part1':
            animatePart1();
			animateRestPart2();
			animateRestPart3();
			animateRestPart4();
			animateRestPart5();
            break;;
        case 'part2':
            animatePart2();
			animateRestPart1();
			animateRestPart3();
			animateRestPart4();
			animateRestPart5();
            break;;
        case 'part3':
            animatePart3();
			animateRestPart2();
			animateRestPart1();
			animateRestPart4();
			animateRestPart5();
            break;;
        case 'part4':
            animatePart4();
			animateRestPart2();
			animateRestPart3();
			animateRestPart1();
			animateRestPart5();
            break;;
        case 'part5':
            animatePart5();
			animateRestPart2();
			animateRestPart3();
			animateRestPart4();
			animateRestPart1();
            break;;
    };
};

function animatePart1() {
	$('#fp-nav ul li a').removeClass('white');
	window.setTimeout(function(){$(".section#part1.active .content").addClass("active");}, 3000);
	window.setTimeout(function(){$(".section#part1.active .animation").addClass("active");}, 4000);
	window.setTimeout(function(){$(".section#part1.active .animation #fork").addClass("active");}, 5000);
	window.setTimeout(function(){$(".section#part1.active .animation #knife").addClass("active");}, 5000);
};
function animateRestPart1() {
	if( ! $("#part1").hasClass("active")){
	//$(".section#part1.active .content").removeClass("active");
	//$(".section#part1.active .animation").removeClass("active");
	////$(".section#part1.active .animation #fork").removeClass("active");
	//$(".section#part1.active .animation #knife").removeClass("active");
	clearInterval();
	}
};
function animatePart2() {
	$('#fp-nav ul li a').addClass('white');
	window.setTimeout(function(){$("#part2 .content div.text img.nobit").addClass("active");}, 2000);
	window.setTimeout(function(){$("#part2 .content div.text img.bit").addClass("active");}, 2000);
	window.setTimeout(function(){$("#part2 .content div.text img#bits").addClass("active");}, 2000);
	window.setTimeout(function(){$("#part2 .content div.text img#bits.active").addClass("animate");}, 2300);
	window.setTimeout(function(){$("#part2 .content").addClass("active");}, 3000);
	window.setTimeout(function(){$(".animation span#map").addClass("active");}, 4000);
	window.setTimeout(function(){$(".animation span#man").addClass("active");}, 4500);
	window.setTimeout(function(){$(".animation span#trail-left-top").addClass("active");}, 5500);
	window.setTimeout(function(){$(".animation span#trail-left-bottom").addClass("active");}, 5500);
	window.setTimeout(function(){$(".animation span#trail-right-top").addClass("active");}, 5500);
	window.setTimeout(function(){$(".animation span#trail-right-bottom").addClass("active");}, 5500);

	window.setTimeout(function(){$(".animation span#burger").addClass("active");}, 7000);
	window.setTimeout(function(){$(".animation span#wine").addClass("active");}, 7000);
	window.setTimeout(function(){$(".animation span#icecream").addClass("active");}, 7000);
	window.setTimeout(function(){$(".animation span#hotdog").addClass("active");}, 7000);



	window.setTimeout(function(){$(".animation span#pointer1").addClass("active");}, 6000);
	window.setTimeout(function(){$(".animation span#pointer2").addClass("active");}, 6000);
	window.setTimeout(function(){$(".animation span#pointer3").addClass("active");}, 6000);

	window.setTimeout(function(){$(".animation span#pointer1.active").addClass("animate");}, 7500);
	window.setTimeout(function(){$(".animation span#pointer2.active").addClass("animate");}, 7600);
	window.setTimeout(function(){$(".animation span#pointer3.active").addClass("animate");}, 7900);
	
}

function animateRestPart2() {
	if( ! $("#part2").hasClass("active")){
	//$("#part2 .content div.text img.nobit").removeClass("active");
//	$("#part2 .content div.text img.bit").removeClass("active");
//	$("#part2 .content div.text img#bits").removeClass("active");
//	$("#part2 .content div.text img#bits.active").removeClass("animate");
//	$("#part2 .content").removeClass("active");
//	$(".animation span#map").removeClass("active");
//	$(".animation span#man").removeClass("active");
//	$(".animation span#trail-left-top").removeClass("active");
//	$(".animation span#trail-left-bottom").removeClass("active");
//	$(".animation span#trail-right-top").removeClass("active");
//	$(".animation span#trail-right-bottom").removeClass("active");
//
//	$(".animation span#burger").removeClass("active");
//	$(".animation span#wine").removeClass("active");
//	$(".animation span#icecream").removeClass("active");
//	$(".animation span#hotdog").removeClass("active");
//
//	$(".animation span#pointer1").removeClass("active");
//	$(".animation span#pointer2").removeClass("active");
//	$(".animation span#pointer3").removeClass("active");
//
//	$(".animation span#pointer1.active").removeClass("animate");
//	$(".animation span#pointer2.active").removeClass("animate");
//	$(".animation span#pointer3.active").removeClass("animate");
	clearInterval();
	}
}
function animatePart3() {
	
	$('#fp-nav ul li a').addClass('white');
   window.setTimeout(function(){$("#part3 .list_carousel ul").addClass("active");}, 1800);
	window.setTimeout(function(){$("#part3 .list_carousel ul.active").addClass("animate");}, 4800);
	window.setTimeout(function(){$("#part3 .list_carousel ul.active.animate").addClass("animate2");}, 7800);
	window.setTimeout(function(){$("#part3 .list_carousel ul.active.animate2").addClass("animate3");}, 10800);
	window.setTimeout(function(){$("#part3 .list_carousel ul.active.animate2.animate3").addClass("animate4");}, 13800);
	window.setTimeout(function(){$("#part3 .list_carousel ulactive.animate2.animate3.animate4").addClass("animate5");}, 16800);
}

function animateRestPart3() {
	if( ! $("#part3").hasClass("active")){
 //   $("#part3 .list_carousel ul").removeClass("active");
//	$("#part3 .list_carousel ul.active").removeClass("animate");
    clearInterval();
	}
}


function animatePart4() {
	$('#fp-nav ul li a').removeClass('white');
	window.setTimeout(function(){$("#part4 .main_align .innerbox").addClass("active");}, 2000);
	window.setTimeout(function(){$("#part4 .main_align .innerbox.active").addClass("active2");}, 3000);
	window.setTimeout(function(){$("#part4 .main_align .iext201").addClass("active");}, 2000);
	window.setTimeout(function(){$("#part4 .main_align .iext202").addClass("active");}, 3000);
	window.setTimeout(function(){$("#part4 .main_align .iext203").addClass("active");}, 4000);
	window.setTimeout(function(){$("#part4 .main_align .iext204").addClass("active");}, 5000);
	window.setTimeout(function(){$("#part4 .main_align .iext205").addClass("active");}, 3500);
	window.setTimeout(function(){$("#part4 .main_align .iext206").addClass("active");}, 4500);
	window.setTimeout(function(){$("#part4 .main_align .iext207").addClass("active");}, 4000);
	window.setTimeout(function(){$("#part4 .main_align .iext208").addClass("active");}, 5000);

	window.setTimeout(function(){$("#part4 .main_align .iext301").addClass("active");}, 2000);
	window.setTimeout(function(){$("#part4 .main_align .iext303").addClass("active");}, 2500);
	window.setTimeout(function(){$("#part4 .main_align .iext304").addClass("active");}, 3500);
	window.setTimeout(function(){$("#part4 .main_align .iext302").addClass("active");}, 3000);
	window.setTimeout(function(){$("#part4 .main_align .iext305").addClass("active");}, 3500);
	window.setTimeout(function(){$("#part4 .main_align .iext306").addClass("active");}, 4500);


	window.setTimeout(function(){$("#part4 .main_align .iext101").addClass("active");}, 2000);
	window.setTimeout(function(){$("#part4 .main_align .iext102").addClass("active");}, 3000);
	window.setTimeout(function(){$("#part4 .main_align .iext103").addClass("active");}, 3300);
	window.setTimeout(function(){$("#part4 .main_align .iext104").addClass("active");}, 4300);
	window.setTimeout(function(){$("#part4 .main_align .iext105").addClass("active");}, 5000);
	window.setTimeout(function(){$("#part4 .main_align .iext106").addClass("active");}, 6000);
	window.setTimeout(function(){$("#part4 .main_align .iext107").addClass("active");}, 3800);
	window.setTimeout(function(){$("#part4 .main_align .iext108").addClass("active");}, 4800);
	window.setTimeout(function(){$("#part4 .main_align .iext109").addClass("active");}, 4500);
	window.setTimeout(function(){$("#part4 .main_align .iext110").addClass("active");}, 5500);
	window.setTimeout(function(){$("#part4 .main_align .iext111").addClass("active");}, 6000);
	window.setTimeout(function(){$("#part4 .main_align .iext112").addClass("active");}, 6500);

}

function animateRestPart4() {

	if( ! $("#part4").hasClass("active")){
	//$("#part4 .innerbox").removeClass("active");
//	$("#part4 .innerbox.active").removeClass("active2");
//	$("#part4 .iext201").removeClass("active");
//	$("#part4 .iext202").removeClass("active");
//	$("#part4 .iext203").removeClass("active");
//	$("#part4 .iext204").removeClass("active");
//	$("#part4 .iext205").removeClass("active");
//	$("#part4 .iext206").removeClass("active");
//	$("#part4 .iext207").removeClass("active");
//	$("#part4 .iext208").removeClass("active");
//
//	$("#part4 .iext301").removeClass("active");
//	$("#part4 .iext303").removeClass("active");
//	$("#part4 .iext304").removeClass("active");
//	$("#part4 .iext302").removeClass("active");
//	$("#part4 .iext305").removeClass("active");
//	$("#part4 .iext306").removeClass("active");
//
//
//	$("#part4 .iext101").removeClass("active");
//	$("#part4 .iext102").removeClass("active");
//	$("#part4 .iext103").removeClass("active");
//	$("#part4 .iext104").removeClass("active");
//	$("#part4 .iext105").removeClass("active");
//	$("#part4 .iext106").removeClass("active");
//	$("#part4 .iext107").removeClass("active");
//	$("#part4 .iext108").removeClass("active");
//	$("#part4 .iext109").removeClass("active");
//	$("#part4 .iext110").removeClass("active");
//	$("#part4 .iext111").removeClass("active");
//	$("#part4 .iext112").removeClass("active");
	clearInterval();
	}
}

function animatePart5() {
	$('#fp-nav ul li a').addClass('white');
	window.setTimeout(function(){$("#part5 .content .google-find #donut").addClass("active");}, 1800);
	window.setTimeout(function(){$("#part5 .scale_image").addClass("active");}, 4800);
	window.setTimeout(function(){$("#part5 .scale_image.active").addClass("active2");}, 5200);
	window.setTimeout(function(){$("#part5 .scale_image.active.active2").addClass("active3");}, 5500);
	window.setTimeout(function(){$("#part5 .scale_image.active.active2.active3").addClass("active4");}, 5800);
	window.setTimeout(function(){$("#part5 .scale_image.active.active2.active3.active4").addClass("active5");}, 6000);
	window.setTimeout(function(){$("#magnifier").addClass("active");}, 6500);
	window.setTimeout(function(){$("#magnifier.active").addClass("active2");}, 7000);
	window.setTimeout(function(){$("#magnifier.active.active2").addClass("active3");}, 7500);
	window.setTimeout(function(){$("#magnifier.active.active2.active3").addClass("active4");}, 8000);
	window.setTimeout(function(){$("#magnifier.active4").addClass("active5");}, 8500);
	window.setTimeout(function(){$("#magnifier.active5").addClass("active6");}, 8900);

	window.setTimeout(function(){$("#sparkle").addClass("active");}, 8900);
	window.setTimeout(function(){$("#zoomeddonut").addClass("active");}, 8900);
}

function animateRestPart5() {
	if(! $("#part5").hasClass("active")){
//	$("#part5 .content .google-find #donut").removeClass("active");
//	$("#part5 .scale_image").removeClass("active");
//	$("#part5 .scale_image.active").removeClass("active2");
//	$("#part5 .scale_image.active.active2").removeClass("active3");
//	$("#part5 .scale_image.active.active2.active3").removeClass("active4");
//	$("#part5 .scale_image.active.active2.active3.active4").removeClass("active5");
//	$("#magnifier").removeClass("active");
//	$("#magnifier.active").removeClass("active2");
//	$("#magnifier.active.active2").removeClass("active3");
//	$("#magnifier.active.active2.active3").removeClass("active4");
//	$("#magnifier.active4").removeClass("active5");
//	$("#magnifier.active5").removeClass("active6");
//
//	$("#sparkle").removeClass("active");
//	$("#zoomeddonut").removeClass("active");
	clearInterval();
	}
}