var mongoClient = require("mongodb").MongoClient
var ObjectId = require("mongodb").ObjectID
var config = require("../commons/config")
var helpers = require("../commons/helpers")

var geocoderProvider = 'google';
var httpAdapter = 'https';
// optionnal
var extra = {
    apiKey: config.google_api, // for Mapquest, OpenCage, Google Premier
    formatter: null         // 'gpx', 'string', ...
};

var geocoder = require('node-geocoder')(geocoderProvider, httpAdapter, extra);

mongoClient.connect("mongodb://"+config.db_host+"/db_hngre",function(err,db){
	if(err) throw err
		else
		{
			console.log("Connected to DB")

			db.collection("users").find({"created":{$gte: new Date(config.date_from+"T00:00:00.000Z")},"email":{$ne:null}},{"first_name":1,"last_name":1,"email":1,}).toArray(function(err,res){
				if(err) throw err
					else
					{
						helpers.asyncLoop(80,function(loop){
						
							db.collection("recommendations").find({"_user":ObjectId(res[loop.iteration()]._id),"params":{$exists:true}}).sort({"time_sent":-1}).toArray(function(err,reccs){
								if(reccs.length > 0)
								{
									
									if(reccs[0].params.lat)
									{
										res[loop.iteration()].location = {
											lat : reccs[0].params.lat,
											lon: reccs[0].params.lon
										}
									}
									if(reccs[0].params.neighbourhood)
									{
										res[loop.iteration()].location = {
												neighbourhood : reccs[0].params.neighbourhood,
												locality : reccs[0].params.locality,
												region : reccs[0].params.region,
												country: reccs[0].params.country

										}
									}
									if(!reccs[0].params.neighbourhood && ! reccs[0].params.lat)
									{
										res[loop.iteration()].location = {
												locality : reccs[0].params.locality,
												region : reccs[0].params.region,
												country: reccs[0].params.country

										}
									}
									console.log(res[loop.iteration()])									
									
								}
								else
								{


								}								
								loop.next()
							})
						},function(){
							console.log("All Done")
						})

						
					}
			})
		}
})


// geocoder.reverse({lat:40.7472184, lon:-74.0059456}, function(err, res) {
//     console.log(res);
// });
