var mongoClient = require("mongodb").MongoClient
var fs = require("fs")
var xlsx = require('node-xlsx')

mongoClient.connect("mongodb://localhost/db_hngre", function(err, db) {
    if (err) console.log(err)
    else {
        console.log("Connected to DB")

        db.collection("merchants").find({
            "address.locality": "Gurgaon"
        }).toArray(function(err, res) {
            if (err) throw err
            else {
                //console.log(res.length)


                var actionsT = xlsx.parse("../data/actions.xlsx")
                var actions = {}
                actionsT[0].data.forEach(function(action) {
                    if (action.length != 0 && action[0] != "zomato_link") {

                        actions[action[0]] = []
                        if (action[1]) {
                            //Food Panda
                            actions[action[0]].push({
                                "name": "Food Panda",
                                "action": "Order",
                                "url": action[1],
                                "icon": "http://d3elexjxzly44z.cloudfront.net/mobile/90x90foodpanda.png"
                            })
                        }
                        if (action[2]) {
                            //zomato
                            actions[action[0]].push({
                                "name": "Zomato",
                                "action": "Order",
                                "url": action[2],
                                "icon": "http://d3elexjxzly44z.cloudfront.net/mobile/90x90zomato.png"
                            })
                        }
                        if (action[3]) {
                            //Dineout
                            actions[action[0]].push({
                                "name": "Dineout",
                                "action": "Reserve",
                                "url": action[3],
                                "icon": "http://d3elexjxzly44z.cloudfront.net/mobile/90x90dineout.  png"
                            })
                        }
                    }
                })



                res.forEach(function(merchant) {

                    var zomatoId = merchant.yelp.link.split("/")[4]

                    if (!zomatoId) {
                        zomatoId = merchant.yelp.link.split("/")[3]
                    }
                    merchant.actions = []

                    if (actions[zomatoId]) {
                        merchant.actions = actions[zomatoId]
                        if (merchant.info.features.indexOf("Seating Available") != -1) {
                            merchant.actions.push({
                                "name": "Uber",
                                "action": "Ride",
                                "url": null,
                                "icon": "http://d3elexjxzly44z.cloudfront.net/mobile/90x90Uber.png"
                            })
                        }
                    } else {
                        if (merchant.info.features.indexOf("Seating Available") != -1) {
                            merchant.actions.push({
                                "name": "Uber",
                                "action": "Ride",
                                "url": null,
                                "icon": "http://d3elexjxzly44z.cloudfront.net/mobile/90x90Uber.png"
                            })
                        }

                    }
                    console.log(merchant.name,merchant.actions)
                    // Insert Uber as Action                       


                    db.collection("merchants").update({
                        "_id": merchant._id
                    }, merchant, function(err, count, body) {
                        if (err) console.log(err)
                        else
                            console.log(count)
                    })

                    // body...
                })
            }
        })
    }
    // body...
})