var mongoClient = require("mongodb").MongoClient
var fs = require("fs")
var xlsx = require('node-xlsx')
var ObjectID = require("mongodb").ObjectID


mongoClient.connect("mongodb://52.77.223.105/db_hngreV2", function(err, db_hngreV2) {
    if (err)
        console.log(err)
    else {
        console.log("Connected t both DB's")
        var dishesT = xlsx.parse("../data/gurgaon.xlsx")
        var dishes = {}
            //console.log(dishesT[0].data[2])
        dishesT[0].data.forEach(function(dish) {
            if (dish.length != 0 && dish[0] != "unique_code") {

                if (dish[4]) {
                    var ingredients = dish[4].split(",")
                } else {
                    var ingredients = []
                }

                if (dish[3]) {
                    var description = dish[3].toString().trim()
                } else {
                    var description = ""
                }
                if (dish[8]) {
                    if (!dishes[dish[8]]) {
                        dishes[dish[8]] = []
                    }

                    dishes[dish[8]].push({
                        name: dish[2],
                        description: description,
                        ingredients: ingredients,
                        veg_type: parseInt(dish[5].toString().trim()),
                        source: {
                            name: dish[7],
                            link: dish[6]
                        },
                        approve_status: true,
                        influencers: [],
                        tags: [],
                        course: [],
                        index: {
                            "umami": 1,
                            "cookedness": 1,
                            "fat": 1,
                            "texture": 1,
                            "chilly": 1,
                            "sour": 1,
                            "sweet": 1
                        },
                        _id: new ObjectID(),
                        photo: {
                            ios: "http://d3qrd3ykbnltrd.cloudfront.net/" + dish[0] + "_ios.jpg",
                            original: "http://d3qrd3ykbnltrd.cloudfront.net/" + dish[0] + ".jpg"
                        }

                    })
                }


            }
        })

        db_hngreV2.collection("merchants_zomato").find().sort({
            "yelp.review_count": -1
        }).toArray(function(err, merchants) {
            if (err) console.log(err)
            else {
                asyncloop(merchants.length, function(loop) {
                    var merchant = merchants[loop.iteration()]
                    var merchant = merchants[loop.iteration()]

                    if (merchant.yelp.link.indexOf("ncr") != -1) {
                        var name = merchant.yelp.link.split("https://www.zomato.com/ncr/")[1]
                    } else {
                        var name = merchant.yelp.link.split("https://www.zomato.com/")[1]
                    }
                    if (!dishes[name]) {
                        //console.log("No Merchant Data on Excel File, move to Next Merchant")
                        loop.next()
                    } else {
                        dishes[name].forEach(function(dish) {
                            dish["cuisine"] = merchant.cuisine
                        })

                        // if (typeof merchant.address.neighbourhood !== "object") {

                        //     merchant.address.neighbourhood = [merchant.address.neighbourhood]
                        // }
                        // merchant.location.coordinates = [parseFloat(merchant.location.coordinates[0]), parseFloat(merchant.location.coordinates[1])]
                            //merchant.location.coordinates = ["77.088608","28.495261"]
                        db_hngreV2.collection("merchants_zomato").update({
                            _id: merchant._id
                        }, {
                            $set: {
                                dishes: dishes[name],
                                "address.neighbourhood": merchant.address.neighbourhood,
                                "location.coordinates": merchant.location.coordinates,
                                "timezone":"Asia/Kolkata"
                            }
                        }, {
                            upsert: true
                        }, function(err, count, res) {
                            // body...
                            if (err) {
                                console.log(err)
                                loop.next()
                            } else {
                                console.log(count)
                                loop.next()
                                    //loop.next()
                            }
                        })
                    }

                }, function() {
                    console.log("All Done")
                    process.exit()
                })
            }
        })


    }
})


function asyncloop(iterations, func, callback) {
    var index = 0;
    var done = false;
    var loop = {
        next: function() {
            if (done) {
                return;
            }

            if (index < iterations) {
                index++;
                func(loop);

            } else {
                done = true;
                callback();
            }
        },

        iteration: function() {
            return index - 1;
        },

        break: function() {
            done = true;
            callback();
        }
    };
    loop.next();
    return loop;
} 