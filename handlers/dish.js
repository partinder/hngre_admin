/**
 * Created by Partinder on 2/11/15.
 */
var get_dishes = require("../methods/get_dishes.js")
var ObjectID = require("mongodb").ObjectID
var remove_duplicates = require("../methods/remove_duplicates")
var s3 = require("s3")
var dish = require("../models/dish")
var fs = require("fs")
var isEmpty = require("../methods/isempty")
var merchant = require("../models/merchant").merchant
var im = require("imagemagick")
var filesizeParser = require('filesize-parser');
var get_merchants = require("../methods/get_merchants")


module.exports = {

    get_handle: function (req, res) {

        if (req.query.dish_id) {

            res.redirect(req.query.dish_id)
        }
        else {
            //console.log(req.params.dish_id)
            //get_dishes.get_one(req.params.dish_id, function (dish) {
            //
            //
            //    get_dishes.get_all(dish._merchant._id, function (dishes) {
            //
            //        //console.log(dishes)
            //        res.render("dish", {user: req.user, page_status: "Dish", dish: dish, all_dishes: dishes})
            //
            //    })
            //
            //})

            merchant.findOne({"dishes":{$elemMatch:{_id: new ObjectID(req.params.dish_id)}}},{"dishes":{$elemMatch:{_id:new ObjectID(req.params.dish_id)}}},function(err, merchantd){
                merchant.findOne({_id : merchantd._id}, function(err,merchant){

                    //var x = []
                    //for(i=0;i<merchant.dishes.length;i++)
                    //{
                    //
                    //
                    //    if(merchant.dishes[i]["approve_status"] === true || merchant.dishes[i]["apprive_status"] == 1)
                    //    {
                    //        x.push(merchant.dishes[i])
                    //    }
                    //
                    //}
                    //
                    //merchant.dishes = x
                    //console.log(merchantd.dishes[0])
                    res.render("dish", {user: req.user, page_status: "Dish", dish: merchantd.dishes[0],merchant:merchant})
                })


            })


                //res.render("dish", {user: req.user, page_status: "Dish", dish: dish, all_dishes: merchant.dishes})


        }


    },
    post_handle: function (req, res) {
        //Start S3 Save
        if (!isEmpty.check(req.files)) {

            var path = req.files.dish_image.path.split(".")
            var newpath= path[0]+"_ios."+path[1]

            var imagename = req.files.dish_image.name.split(".")
            var newname = imagename[0]+"_ios."+imagename[1]

            //var filename = req.files.dish_image.name
            //console.log(req.files.dish_image.name)

            console.log(imagename,newname)


            var image_features = {}

            im.identify(req.files.dish_image.path, function(err, features){
                if (err)
                {
                    console.log("Some Err")
                    throw err
                }

                image_features = features
                // { format: 'JPEG', width: 3904, height: 2622, depth: 8 }

                var image_quality = features.quality

                if(filesizeParser(image_features.filesize) > 102400)
                {
                    image_quality = .7
                }

                im.resize({
                        srcPath: req.files.dish_image.path,
                        dstPath: newpath,
                        quality: image_quality,
                        width: image_features.width,
                        sharpening: 0.0
                    }, function(err, stdout, stderr){
                        if (err) throw err;
                        console.log('File reduced');

                    var client = s3.createClient({
                        maxAsyncS3: 20,     // this is the default
                        s3RetryCount: 3,    // this is the default
                        s3RetryDelay: 1000, // this is the default
                        multipartUploadThreshold: 20971520, // this is the default (20 MB)
                        multipartUploadSize: 15728640, // this is the default (15 MB)
                        s3Options: {
                            accessKeyId: "AKIAJLEJXUICVBQU7RMQ",
                            secretAccessKey: "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"
                            // any other options are passed to new AWS.S3()
                            // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Config.html#constructor-property
                        }
                    });

                    var params = {
                        localFile: req.files.dish_image.path,

                        s3Params: {
                            Bucket: "hngredishes",
                            Key: req.files.dish_image.name
                            // other options supported by putObject, except Body and ContentLength.
                            // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
                        }
                    };

                    var params1 = {
                        localFile: newpath,

                        s3Params: {
                            Bucket: "hngredishes",
                            Key: newname
                            // other options supported by putObject, except Body and ContentLength.
                            // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
                        }
                    };

                    console.log(newpath)



                    var uploader = client.uploadFile(params1);
                    //uploader = client.uploadFile(params)
                    uploader.on('error', function (err) {
                        console.error("unable to upload:", err.stack);
                    });
                    uploader.on('progress', function () {
                        console.log("progress", uploader.progressMd5Amount,
                            uploader.progressAmount, uploader.progressTotal);
                    });
                    uploader.on('end', function () {
                        console.log("done uploading reduced");

                        fs.unlink(newpath, function (err) {
                            if (err) throw err;
                            //res.send('File uploaded to: ' + target_path + ' - ' + req.files.dish_image.thumbnail.size + ' bytes');
                            else console.log("Reduced File Uploaded to S3 and local deleted")
                        });


                        req.body.tags = remove_duplicates.remove_duplicates(req.body.tags)
                        req.body.course = remove_duplicates.remove_duplicates(req.body.course)
                        req.body.ingredients = remove_duplicates.remove_duplicates(req.body.ingredients)
                        req.body.influencers = remove_duplicates.remove_duplicates(req.body.influencers)
                        req.body.cuisine = remove_duplicates.remove_duplicates(req.body.cuisine)

                        var data = req.body

                        var photos ={
                            original: "http://d3qrd3ykbnltrd.cloudfront.net/" + req.files.dish_image.name,
                            ios: "http://d3qrd3ykbnltrd.cloudfront.net/" + newname

                        }
                        console.log("UPDATED COST IS : %s",req.body.cost)
                        merchant.update({"dishes":{$elemMatch:{_id:new ObjectID(req.params.dish_id)}}},
                            {
                                "$set":{

                                    "dishes.$.name" : data.name,
                                    "dishes.$.description" : data.description,
                                    "dishes.$.cuisine" : data.cuisine,
                                    "dishes.$.sub_cuisine" : data.sub_cuisine,
                                    "dishes.$.ingredients" : data.ingredients,
                                    "dishes.$.course" : data.course,
                                    "dishes.$.veg_type" : parseInt(data.veg_type),
                                    "dishes.$.tags" : data.tags,
                                    "dishes.$.index.fat" : parseInt(data.index.fat),
                                    "dishes.$.index.sweet" : parseInt(data.index.sweet),
                                    "dishes.$.index.sour" : parseInt(data.index.sour),
                                    "dishes.$.index.chilly" : parseInt(data.index.chilly),
                                    "dishes.$.index.cookedness" : parseInt(data.index.cookedness),
                                    "dishes.$.index.umami" : parseInt(data.index.umami),
                                    "dishes.$.index.texture" : parseInt(data.index.texture),
                                    "dishes.$.approve_status" : Boolean(data.approve_status),
                                    "dishes.$.influencers" : data.influencers,
                                    "dishes.$.photo" : photos,
                                    "dishes.$.cost.value": parseInt(data.cost)

                                }
                            },
                            function (err, merchant1){
                                if(err){}
                                else
                                {
                                    merchant.findOne({"dishes":{$elemMatch:{_id: new ObjectID(req.params.dish_id)}}},{"dishes":{$elemMatch:{_id:new ObjectID(req.params.dish_id)}}},function(err, merchantd){

                                        //console.log(merchantd)
                                        merchant.findOne({_id : merchantd._id}, function(err,merchant1){
                                            res.render("dish", {user: req.user, page_status: "Dish", dish: merchantd.dishes[0],merchant:merchant1})
                                            merchant.elasticUpdate(merchant1)
                                        })



                                    })
                                }
                            }
                        )
                    });


                    var uploader1 = client.uploadFile(params)
                    uploader1.on('error', function (err) {
                        console.error("unable to upload:", err.stack);
                    });
                    uploader1.on('progress', function () {
                        console.log("progress", uploader.progressMd5Amount,
                            uploader.progressAmount, uploader.progressTotal);
                    });
                    uploader1.on('end', function () {
                        console.log("done uploading");
                        fs.unlink(req.files.dish_image.path, function (err) {
                            if (err) throw err;
                            //res.send('File uploaded to: ' + target_path + ' - ' + req.files.dish_image.thumbnail.size + ' bytes');
                            else console.log("Original File Uploaded to S3 and local deleted")
                        });
                    })






                });




            });



        } else {
            //Array fields Handling
            //console.log(req.body)

            req.body.tags = remove_duplicates.remove_duplicates(req.body.tags)
            req.body.course = remove_duplicates.remove_duplicates(req.body.course)
            req.body.ingredients = remove_duplicates.remove_duplicates(req.body.ingredients)
            req.body.influencers = remove_duplicates.remove_duplicates(req.body.influencers)
            req.body.cuisine = remove_duplicates.remove_duplicates(req.body.cuisine)

            var data = req.body
            merchant.update({"dishes":{$elemMatch:{_id:new ObjectID(req.params.dish_id)}}},{
                "$set":{

                    'dishes.$.name' : data.name,
                    "dishes.$.description" : data.description,
                    "dishes.$.cuisine" : data.cuisine,
                    "dishes.$.sub_cuisine" : data.sub_cuisine,
                    "dishes.$.ingredients" : data.ingredients,
                    "dishes.$.course" : data.course,
                    "dishes.$.veg_type" : parseInt(data.veg_type),
                    "dishes.$.tags" : data.tags,
                    "dishes.$.index.fat" : parseInt(data.index.fat),
                    "dishes.$.index.sweet" : parseInt(data.index.sweet),
                    "dishes.$.index.sour" : parseInt(data.index.sour),
                    "dishes.$.index.chilly" : parseInt(data.index.chilly),
                    "dishes.$.index.cookedness" : parseInt(data.index.cookedness),
                    "dishes.$.index.umami" : parseInt(data.index.umami),
                    "dishes.$.index.texture" : parseInt(data.index.texture),
                    "dishes.$.approve_status" : Boolean(data.approve_status),
                    "dishes.$.influencers" : data.influencers,
                    "dishes.$.cost.value": parseInt(data.cost)
                    //"dishes.$.photo" : "https://s3.amazonaws.com/hngredishes/" + req.files.dish_image.name

                }},
                function(err, merchant1){
                    if(err){}
                    else
                    {
                        merchant.findOne({"dishes":{$elemMatch:{_id: new ObjectID(req.params.dish_id)}}},{"dishes":{$elemMatch:{_id:new ObjectID(req.params.dish_id)}}},function(err, merchantd){

                            //console.log(merchantd)

                            merchant.findOne({_id : merchantd._id}, function(err,merchant1){
                                res.render("dish", {user: req.user, page_status: "Dish", dish: merchantd.dishes[0],merchant:merchant1})

                                merchant.elasticUpdate(merchant1)
                            })


                        })
                    }
                }
            )

        }
        //End S3 save

        //res.send("OK")

    },
    delete_handle: function (req, res) {

        console.log("dish id to be deleted : " + req.params.dish_id)


        merchant.update({"dishes":{$elemMatch:{_id:new ObjectID(req.params.dish_id)}}},{
            $set:{
                "dishes.$.approve_status" : false
            }
        },function(err,merchant1){
            if(err)
            {

            }
            else
            {

                merchant.findOne({"dishes":{$elemMatch:{_id: new ObjectID(req.params.dish_id)}}},function(err, merchantd){

                    res.redirect("/admin/merchants/" + merchantd._id)
                    merchant.elasticUpdate(merchantd)

                })
            }

        })



    }
}
