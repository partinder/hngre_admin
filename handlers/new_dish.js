/**
 * Created by Partinder on 2/19/15.
 */

/**
 * Created by Partinder on 2/11/15.
 */
var get_merchants = require("../methods/get_merchants")
var remove_duplicates = require("../methods/remove_duplicates")
var s3 = require("s3")
var dish = require("../models/dish")
var fs = require("fs")
var isEmpty =require("../methods/isempty")
var merchant = require("../models/merchant").merchant
var mongoose = require("mongoose")

var im = require("imagemagick")
var filesizeParser = require('filesize-parser');

dish =  mongoose.model('Dish', dish.schema);

//var Mongoose = require("mongoose")
//var connection = Mongoose.createConnection('mongodb://localhost/db_hngre');
//var dish_new= connection.model('Dish', dish.schema);





module.exports ={



    get_handle : function(req,res){
        //console.log(new_dish)

        if(req.query.merchant_id){

            console.log("dish here")
            res.redirect(req.query.merchant_id)
        }
        else
        {
            get_merchants.get_one(req.params.merchant_id, function(merchant){

                    //console.log(dishes)
                    res.render("new_dish",{user:req.user,page_status:"Dish", merchant:merchant, all_dishes:merchant.dishes})

            })

        }





    },
    post_handle: function (req, res) {

        var backURL1 = req.header("Referer")

        if(req.header("Refered")){
            if(backURL1.search("crawled") != -1)
            {

                backURL1 = "Crawled"

            }
            if(backURL1.search("open") != -1)
            {
                backURL1 = "Open"

            }

        }
        //Start S3 Save
        if (!isEmpty.check(req.files)) {


            var path = req.files.dish_image.path.split(".")
            var newpath= path[0]+"_ios."+path[1]

            var imagename = req.files.dish_image.name.split(".")
            var newname = imagename[0]+"_ios."+imagename[1]

            //var filename = req.files.dish_image.name
            console.log(req.files.dish_image.name)


            var image_features = {}

            im.identify(req.files.dish_image.path, function(err, features) {
                if (err) throw err;

                image_features = features
                // { format: 'JPEG', width: 3904, height: 2622, depth: 8 }

                var image_quality = features.quality

                if (filesizeParser(image_features.filesize) > 102400) {
                    image_quality = .7
                }

                im.resize({
                    srcPath: req.files.dish_image.path,
                    dstPath: newpath,
                    quality: image_quality,
                    width: image_features.width,
                    sharpening: 0.0
                }, function (err, stdout, stderr) {
                    if (err) throw err;
                    console.log('File reduced');


                    var client = s3.createClient({
                        maxAsyncS3: 20,     // this is the default
                        s3RetryCount: 3,    // this is the default
                        s3RetryDelay: 1000, // this is the default
                        multipartUploadThreshold: 20971520, // this is the default (20 MB)
                        multipartUploadSize: 15728640, // this is the default (15 MB)
                        s3Options: {
                            accessKeyId: "AKIAJLEJXUICVBQU7RMQ",
                            secretAccessKey: "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"
                            // any other options are passed to new AWS.S3()
                            // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Config.html#constructor-property
                        }
                    });

                    var params = {
                        localFile: req.files.dish_image.path,

                        s3Params: {
                            Bucket: "hngredishes",
                            Key: req.files.dish_image.name
                            // other options supported by putObject, except Body and ContentLength.
                            // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
                        }
                    };

                    var params1 = {
                        localFile: newpath,

                        s3Params: {
                            Bucket: "hngredishes",
                            Key: newname
                            // other options supported by putObject, except Body and ContentLength.
                            // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
                        }
                    };

                    //console.log(newpath)


                    var uploader = client.uploadFile(params1);
                    //uploader = client.uploadFile(params)
                    uploader.on('error', function (err) {
                        console.error("unable to upload:", err.stack);
                    });
                    uploader.on('progress', function () {
                        console.log("progress", uploader.progressMd5Amount,
                            uploader.progressAmount, uploader.progressTotal);
                    });
                    uploader.on('end', function () {

                        fs.unlink(newpath, function (err) {
                            if (err) throw err;
                            //res.send('File uploaded to: ' + target_path + ' - ' + req.files.dish_image.thumbnail.size + ' bytes');
                            else console.log(" Reduced File Uploaded tp S3 and local deleted")
                        });


                        req.body.tags = remove_duplicates.remove_duplicates(req.body.tags)
                        req.body.course = remove_duplicates.remove_duplicates(req.body.course)
                        req.body.ingredients = remove_duplicates.remove_duplicates(req.body.ingredients)
                        req.body.influencers = remove_duplicates.remove_duplicates(req.body.influencers)
                        req.body.cuisine = remove_duplicates.remove_duplicates(req.body.cuisine)





                        var data = req.body
                        var new_dish = new dish({
                            name: data.name,
                            description: data.description,
                            cuisine: data.cuisine,
                            sub_cuisine: data.sub_cuisine,
                            ingredients: data.ingredients,
                            course: data.course,
                            veg_type: parseInt(data.veg_type),
                            tags: data.tags,
                            index: {
                                sweet: parseInt(data.index.sweet),
                                sour: parseInt(data.index.sour),
                                chilly: parseInt(data.index.chilly),
                                texture: parseInt(data.index.texture),
                                fat: parseInt(data.index.fat),
                                cookedness: parseInt(data.index.cookedness),
                                umami: parseInt(data.index.umami)
                            },
                            approve_status: parseInt(data.approve_status),
                            influencers: data.influencers,
                            photo: {
                                original: "http://d3qrd3ykbnltrd.cloudfront.net/" + req.files.dish_image.name,
                                ios: "http://d3qrd3ykbnltrd.cloudfront.net/" + newname

                            }

                        })
                        //"dishes.$.photo" : "https://s3.amazonaws.com/hngredishes/" + req.files.dish_image.name


                        merchant.findOne({_id: req.params.merchant_id}, function (err, merchant_found) {




                            merchant_found.dishes.push(new_dish)
                            merchant_found.save(function (err) {
                                if (err) {

                                }
                                else {
                                    //console.log(new_dish)
                                    res.redirect("/admin/merchants/" + req.params.merchant_id)
                                    //res.render("merchant",{user: req.user,merchant: merchant_found, page_status:backURL1,dishes:merchant_found.dishes})
                                }
                            })
                        })


                    })


                    var uploader1 = client.uploadFile(params)
                    uploader1.on('error', function (err) {
                        console.error("unable to upload:", err.stack);
                    });
                    uploader1.on('progress', function () {
                        console.log("progress", uploader.progressMd5Amount,
                            uploader.progressAmount, uploader.progressTotal);
                    });
                    uploader1.on('end', function () {
                        console.log("done uploading");
                        fs.unlink(req.files.dish_image.path, function (err) {
                            if (err) throw err;
                            //res.send('File uploaded to: ' + target_path + ' - ' + req.files.dish_image.thumbnail.size + ' bytes');
                            else console.log("Original File Uploaded to S3 and local deleted")
                        });
                    })


                })


                // delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted fil}
            })
            } else {

            req.body.tags = remove_duplicates.remove_duplicates(req.body.tags)
            req.body.course = remove_duplicates.remove_duplicates(req.body.course)
            req.body.ingredients = remove_duplicates.remove_duplicates(req.body.ingredients)
            req.body.influencers = remove_duplicates.remove_duplicates(req.body.influencers)
            req.body.cuisine = remove_duplicates.remove_duplicates(req.body.cuisine)

            var data = req.body
            var new_dish = new dish({
                name : data.name,
                description : data.description,
                cuisine : data.cuisine,
                sub_cuisine : data.sub_cuisine,
                ingredients : data.ingredients,
                course : data.course,
                veg_type : parseInt(data.veg_type),
                tags : data.tags,
                index :{
                    sweet : parseInt(data.index.sweet),
                    sour: parseInt(data.index.sour),
                    chilly : parseInt(data.index.chilly),
                    texture: parseInt(data.index.texture),
                    fat: parseInt(data.index.fat),
                    cookedness: parseInt(data.index.cookedness),
                    umami: parseInt(data.index.umami)
                },
                approve_status : parseInt(data.approve_status),
                influencers : data.influencers,


            })

            merchant.findOne({_id: req.params.merchant_id}, function(err,merchant_found){
                merchant_found.dishes.push(new_dish)
                merchant_found.save(function(err){
                    if(err){

                    }
                    else
                    {
                        res.redirect("/admin/merchants/"+req.params.merchant_id)
                        //res.render("merchant",{user: req.user,merchant: merchant_found, page_status:backURL1,dishes:merchant_found.dishes})
                    }
                })
            })





        }
        //End S3 save

        //res.send("OK")

    }


}



