/**
 * Created by Partinder on 2/2/15.
 */

var merchant = require("../models/merchant.js").merchant
var _ = require("underscore")


module.exports = {

    get_handle: function(req, res) {

        var backURL1 = req.headers.referer
        console.log(backURL1)
        console.log(req.query.term)
        var merchant_status = String


        if (backURL1.search("crawled") != -1) {

            backURL1 = "pool"

        }
        if (backURL1.search("open") != -1) {
            backURL1 = "open"

        }

        if (backURL1.search("completed") != -1) {
            backURL1 = "completed"

        }

        console.log(req.query.term)

        var regex = new RegExp(req.query.term, 'i')

        var query = merchant.find({ $or: [{ "name": regex }, { "address.neighbourhood": regex }, { "dishes.name": regex }] }).where({ status: backURL1 }).sort({ "yelp.review_count": -1 }).select("name address.zip address.neighbourhood dishes").limit(10);

        // Execute query in a callback and return users list

        query.exec(function(err, merchants) {

            if (!err) {
                // Method to construct the json result set
                var results = buildjson(regex, merchants)


                results = JSON.stringify(results)
                console.log(results)

                res.send(results, {
                    'Content-Type': 'application/json'
                }, 200);
            } else {
                res.send(JSON.stringify(err), {
                    'Content-Type': 'application/json'
                }, 404);
            }
        })

    }
}



function buildjson(regex, merchants) {


    console.log(merchants)
    var result = []
    merchants.forEach(function(merchant) {
        var name = merchant.name
        var zip = merchant.address.zip


        if (name.match(regex)) {

            var new_json = {
                label: merchant.name

            }
            result.push(new_json)
        } else {


            merchant.address.neighbourhood.forEach(function(nei) {

                if (nei.match(regex)) {
                    var new_json = {
                        label: nei
                    }
                    result.push(new_json)


                } else {
                    merchant.dishes.forEach(function(dish) {
                        if (dish.name.match(regex)) {
                            var new_json = {
                                label: dish.name

                            }
                            result.push(new_json)
                        }
                    })
                }


            })



        }
    })

    result = result.sort(function(a, b) {
        var labelA = a.label.toLowerCase(),
            labelB = b.label.toLowerCase()
        if (labelA < labelB) //sort string ascending
            return -1
        if (labelA > labelB)
            return 1
        return 0 //default return value (no sorting)

    })


    for (var i = 0; i < result.length - 1; i++) {
        if (result[i].label == result[i + 1].label) {
            delete result[i];
        }
    }

    result = result.filter(function(el) {
        return (typeof el !== "undefined");
    });

    console.log(result)
    return result
}










//var found = str.match(regex)