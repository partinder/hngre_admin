/**
 * Created by Partinder on 5/11/15.
 */

var  MongoClient = require("mongodb").MongoClient
var ObjectID = require('mongodb').ObjectID
var remove_duplicates = require("../methods/remove_duplicates")

module.exports = {

    get_handle : function(req,res){

        MongoClient.connect('mongodb://localhost/db_hngre', function (err, db) {


            if (err) {
                throw err
            }
            else {

                var query = req.query.count
                var count = 1
                if(query)
                {
                    count = parseInt(query)
                }

                var users = db.collection("dumy_users")
                users.find({"int_count":{$gte:count}},{name:1, photo:1,total_review_count:1,location:1,int_count:1,status:1 },{"sort": [['int_count','desc']]},function(err,cursor){
                    if(err)
                    {

                    }
                    else
                    {
                       cursor.toArray(function(err,array){
                           if(err)
                           {

                           }
                           else
                           {

                               res.render("reviewer_list",{user:req.user,review_users:array,page_status: "reviews"})
                               //console.log(array.length)
                           }
                       })

                    }
                })

            }
        })
    },
    get_handle_user: function(req,res){
        var user_id = req.params.id

        MongoClient.connect("mongodb://localhost/db_hngre", function(err,db){
            if(err)
            {
                console.log(err)
            }
            else
            {
                var user = db.collection("dumy_users")
                var ratings = db.collection("ratings")
                var dishes = []
                var merchants = []
                user.findOne({"_id":ObjectID(user_id)},function(err,user_found){
                    if(err)
                    {
                        console.log(err)
                    }
                    else
                    {
                        //console.log(user_found.status)

                        var merchant = db.collection("merchants")
                        asyncLoop(user_found.reviews.length,function(loop){
                            merchant.findOne({"yelp.link":user_found.reviews[loop.iteration()].rest_yelp},{"name":1,"dishes":1},function(err,result){

                                user_found.reviews[loop.iteration()].merchant = result

                                loop.next()

                            })
                        },function(){

                            //res.send(user_found.reviews)
                            user_found.reviews.forEach(function(review){
                                merchants.push(ObjectID(review.merchant._id))
                                review.review = review.review.replace(/[&\/\\#+()$~%'":*?<>{}]/g,' ');

                                var dish_name =[]
                                review.merchant.dishes.forEach(function(dish){
                                    dishes.push(ObjectID(dish._id))

                                    var dish1 = dish.name.split(" ")

                                    dish1.forEach(function(name){

                                            dish_name.push(name)
                                    })
                                })


                                dish_name = remove_duplicates.remove_duplicates(dish_name)
                                var count =0

                                dish_name.forEach(function(name_tofind){

                                    name_tofind = name_tofind.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'');

                                    //name_tofind= name_tofind.replace(" ","")


                                        if(name_tofind.length != 0)
                                        {
                                            //console.log(count, name_tofind)
                                            var regex = new RegExp("(?!the|for|with|and|on|of|style)\\b("+name_tofind+")\\b", 'gi')

                                            review.review = review.review.replace(regex,"<span style='background-color: #F5D76E'>"+name_tofind+"</span>")
                                        }





                                    if(review.review.search(regex)!= -1)
                                    {
                                        review.merchant.dishes.forEach(function(dish){

                                            dish.name = dish.name.replace(regex,"<span style='background-color: #A2DED0;font-weight: 900;font-size:16px'>"+name_tofind+"</span>")
                                        })
                                    }

                                })

                            })

                               //console.log(dishes)
                            ratings.find({"_user":ObjectID(user_id),"_dish":{$in:dishes},"_merchant":{$in:merchants}},function(err,cursor){
                                if(err)
                                {
                                    console.log(err)
                                }
                                else
                                {
                                    cursor.toArray(function(err,array){
                                        if(err)
                                        {
                                            console.log(err)
                                        }
                                        else
                                        {
                                            //console.log(user_found)
                                            res.render("user_reviews.ejs",{user:req.user,reviewer:user_found,page_status:"reviews",ratings:array})
                                        }
                                    })
                                }

                            })
                            //console.log(dishes.length)


                        })

                    }
                })
            }
        })
    },
    get_rating: function(req,res){
        //console.log(req.query)
        MongoClient.connect("mongodb://localhost/db_hngre",function(err,db){
            if(err)
            {
                throw  err
            }
            else
            {
                var rating = db.collection("ratings")

                var new_rating = {
                    _user: ObjectID (req.query.user),
                    _dish: ObjectID(req.query.dishid),
                    rating:req.query.rating,
                    _merchant: ObjectID(req.query.merchantid)
                }

                //console.log(new_rating)
                //console.log(new_rating)
                rating.update({_user:ObjectID(req.query.user),_dish:ObjectID(req.query.dishid),_merchant:ObjectID(req.query.merchantid)},new_rating,{upsert:true},function(err,sta){
                    if(err)
                    {
                        res.send(err)
                    }
                    else
                    {
                        //console.log(sta)
                        res.send("OK DOne")
                        console.log("Rating Saved",new_rating)
                        var dumy_user = db.collection("dumy_users")
                        dumy_user.findAndModify({"_id":ObjectID(req.query.user)},[["name",1]],{$set:{"status":1}},function(err,res){
                            if(err)
                            {
                                throw err
                            }
                            else
                            {
                                console.log(res.status)
                            }
                        })
                    }
                })
            }
        })
    }


}

function asyncLoop(iterations, func, callback) {
    var index = 0;
    var done = false;
    var loop = {
        next: function() {
            if (done) {
                return;
            }

            if (index < iterations) {
                index++;
                func(loop);

            } else {
                done = true;
                callback();
            }
        },

        iteration: function() {
            return index - 1;
        },

        break: function() {
            done = true;
            callback();
        }
    };
    loop.next();
    return loop;
}

//MongoClient.connect('mongodb://52.5.0.16/db_hngre', function (err, db) {

