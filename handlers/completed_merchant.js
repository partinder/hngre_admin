/**
 * Created by Partinder on 2/23/15.
 */
var get_merchants = require("../methods/get_merchants.js")
var math = require("mathjs")
var merchant= require("../models/merchant.js").merchant

module.exports ={

    get_handle: function (req, res) {



        var sort_by = ""
        var search_query = ""
        var current_page = 1
        var has_previous = false
        var has_next = false

        //console.log("search query is : "+req.query.search)
        //console.log(req.query.sort)
        if (req.query.sort) {
            sort_by = req.query.sort
        }

        else {
            sort_by = "yelp.review_count"
        }


        if (req.query.search) {
            search_query = req.query.search
        }

        if (req.query.page) {
            current_page = parseInt(req.query.page)
        }

        //current_page = current_page


        get_merchants.get_all(sort_by, search_query, current_page, "completed", function (merchants, total_merchants) {

            var total_pages = Math.ceil(total_merchants / 10)
            console.log("Total Pages: " + total_pages)
            console.log("Current Page : " + current_page)
            var start_page = Number
            var end_page = Number

            if (current_page / 10 < 1) {
                var current_page1 = current_page / 10 + 1
                start_page = math.floor(current_page1)
                end_page = start_page + 8

            }
            else {
                start_page = math.floor(current_page / 10)
                start_page = start_page * 10
                end_page = start_page + 9
            }

            if (end_page > total_pages) {
                end_page = total_pages
            }

            if (end_page < total_pages) {
                has_next = true
            }
            if (current_page > 10 || current_page === 10) {
                has_previous = true
            }

            if(req.query.search)
            {
                req.query.search = req.query.search.toString().replace(" ","%20")
            }

            console.log("Start Page : " + start_page)
            console.log("End Page : " + end_page)


            //console.log(merchants)

            res.render("completed_merchant", {
                user: req.user,
                merchants: merchants,
                page_status: "Completed",
                total_pages: total_pages,
                current_page: current_page,
                start_page: start_page,
                end_page: end_page,
                search_query: req.query.search,
                has_next: has_next,
                has_previous: has_previous,
                total_merchants : total_merchants
            })

        })


    },
    post_handle : function(req,res){

    }
}