/**
 * Created by Partinder on 2/4/15.
 */


var get_merchants = require("../methods/get_merchants.js")
var get_dishes = require("../methods/get_dishes.js")
var post_merchants = require("../methods/post_merchants.js")
var merchant = require("../models/merchant.js").merchant
var remove_duplicates = require("../methods/remove_duplicates")


module.exports ={

    get_details : function(req,res){

        var backURL1 = req.header("Referer")

        if(req.header("Refered")){
            if(backURL1.search("crawled") != -1)
            {

                backURL1 = "Crawled"

            }
            if(backURL1.search("open") != -1)
            {
                backURL1 = "Open"

            }

        }



        //console.log(backURL1)

        get_merchants.get_one(req.params.id, function(merchantd){

           //var dishes =[]
           //
           for(i=0;i<merchantd.dishes.length;i++){
                //console.log(merchantd.dishes[i].approve_status)
                //if(merchantd.dishes[i].approve_status === true)
                //{
                //    dishes.push(merchantd.dishes[i])
                //
                //}

           }
              //console.log(merchantd.dishes.length)
                res.render("merchant",{user: req.user,merchant: merchantd, page_status:backURL1,dishes:merchantd.dishes})

            //console.log(merchant)
        })//end get_merchants



    },
    post_details : function(req,res){

        var backURL1 = req.header('Referer')
        var merchant_status = String


        if(backURL1.search("crawled") != -1)
        {

            backURL1 = "pool"

        }
        if(backURL1.search("open") != -1)
        {
            backURL1 = "open"

        }
        //console.log(req.body)
        //console.log(req.body.address.neighbourhood.length)


        //console.log(req.body.address.neighbourhood)
        req.body.address.neighbourhood = remove_duplicates.remove_duplicates(req.body.address.neighbourhood)
        //console.log(req.body.address.neighbourhood)
        req.body.cuisine = remove_duplicates.remove_duplicates(req.body.cuisine)
        req.body.tags = remove_duplicates.remove_duplicates(req.body.tags)
        req.body.influencers = remove_duplicates.remove_duplicates(req.body.influencers)
        req.body.info.meal = remove_duplicates.remove_duplicates(req.body.info.meal)
        req.body.info.ambience = remove_duplicates.remove_duplicates(req.body.info.ambience)
        req.body.contact.phone = remove_duplicates.remove_duplicates(req.body.contact.phone)
        req.body.contact.email = remove_duplicates.remove_duplicates(req.body.contact.email)


        //console.log(req.body.address.neighbourhood)

        merchant.findById(req.params.id,function(err,merchant1){

            var data = req.body

            //for(var key in data){
            //    console.log(key)
            //}
            //
            //console.log(data.info.hours)

            merchant1.name = data.name
            merchant1.contact.primary_email = data.contact.primary_email
            merchant1.contact.phone = data.contact.phone
            merchant1.contact.email = data.contact.email
            merchant1.url = data.url
            merchant1.address.street_address = data.address.street_address
            merchant1.address.locality = data.address.locality
            merchant1.address.region = data.address.region
            merchant1.address.zip = data.address.zip
            merchant1.address.landmark = data.address.landmark
            merchant1.address.neighbourhood = data.address.neighbourhood
            merchant1.yelp.link = data.yelp.link
            merchant1.yelp.rating = data.yelp.rating
            merchant1.yelp.review_count = data.yelp.review_count
            merchant1.zagat.food = data.zagat.food
            merchant1.zagat.service = data.zagat.service
            merchant1.zagat.decor = data.zagat.decor
            merchant1.cuisine = data.cuisine
            merchant1.tags = data.tags
            merchant1.influencers = data.influencers
            merchant1.status = data.status

            merchant1.info.meal = data.info.meal
            merchant1.info.ambience = data.info.ambience
            merchant1.info.credit_card = data.info.credit_card
            merchant1.info.groups = data.info.groups
            merchant1.info.kids = data.info.kids
            merchant1.info.reservations = data.info.reservations
            merchant1.info.table_service = data.info.table_service
            merchant1.info.outdoor_seating = data.info.outdoor_seating
            merchant1.info.wifi = data.info.wifi
            merchant1.info.smoking = data.info.smoking
            merchant1.info.coat = data.info.coat
            merchant1.info.wheelchair = data.info.wheelchair
            merchant1.info.tv = data.info.tv
            merchant1.info.caters = data.info.caters
            merchant1.info.attire = data.info.attire
            merchant1.info.price_range = data.info.price_range
            merchant1.info.music = data.info.music
            merchant1.info.noise = data.info.noise
            merchant1.info.alcohol = data.info.alcohol
            merchant1.info.alcohol_byo = data.info.alcohol_byo

            //console.log(merchant.address.neighbourhood)


            merchant1.save(function(err) {
                if (err) {
                    console.log(err)
                }
                //get_dishes.get_all(req.params.id, function(dishes){
                //    console.log(data.status)

                merchant.elasticUpdate(merchant1)

                res.render("merchant", {
                    user: req.user,
                    merchant: merchant1,
                    page_status: data.status,
                    dishes: merchant1.dishes
                })


                })



            //console.log(merchant)





        })




    }//end get_details
}//end exports

