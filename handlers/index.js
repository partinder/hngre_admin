/*
    INDEX
    Handles for the Index Routes
 */

module.exports =  {

    get_handle : function (req, res){

        if(req.isAuthenticated()){

           //console.log(req.originalUrl)
            res.redirect("/admin/merchants/crawled")
        }
        else
        {
            res.render("index",{message: req.flash("loginMessage")})
        }



    },

    post_handle : function(req,res){

        console.log(req.originalUrl)

        //if(req.originalUrl)
        //{
        //    res.redirect(req.originalUrl)
        //}
        res.redirect("/admin/merchants/crawled")

    }


}
